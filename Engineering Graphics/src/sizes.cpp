#include <stdio.h>
#include <stdlib.h>

int main() {
    float base, real, proportion;

    system("cls");

    printf("Enter base dimension:\t");
    scanf("%f", &base); getchar();
    printf("Enter real dimension:\t");
    scanf("%f", &real); getchar();
    printf("Enter proportion:\t");
    scanf("%f", &proportion); getchar();
    for (putchar('\n'); ; ) {
        float result;

        printf("Enter dimension for count:\t");
        scanf("%f", &result); getchar();

        result = base / real * result * proportion;
        printf("Approximately\t\t%f\n", result);
    }

    return 0;
}