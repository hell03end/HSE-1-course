# Autocad works
Данный раздел содержит выполнение заданий по курсу «Компьютерный практикум по инженерной графике 1 курс (code 64729)» (НИУ ВШЭ, 2015-16). Содержимое раздела распространяется на условиях лицензии [MIT License](https://github.com/hell03end/HSE-1-course/blob/master/LICENSE). При использовании материалов обязательно упоминание автора работ. При наличии технической возможности необходимо также указать активную гиперссылку на [репозиторий автора](https://github.com/hell03end/) или, собственно, на [данный раздел](https://github.com/hell03end/HSE-1-course/tree/master/Engineering%20Graphics).

**Made by [hell03end](https://github.com/hell03end/).**

*All works were done by me (even if it stated otherwise).*

2015-2016.
