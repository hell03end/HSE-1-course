#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>

#define SUBSTRING { str_numb[*sub_n] = i; str_numb[*sub_n] = i; strncpy(sub_str[*sub_n], begin_pointer, str_pointer - begin_pointer); \
if (sub_str[*sub_n][str_pointer - begin_pointer - 1] == '\n') sub_str[*sub_n][str_pointer - begin_pointer - 1] = '\0'; \
else sub_str[*sub_n][str_pointer - begin_pointer] = '\0'; *sub_n += (string_is_not_empty(sub_str[*sub_n])) ? 1 : 0; };

typedef char String[81];
enum maximums { STRINGS = 10, SUBSTRINGS = 30, LENGTH = 81 };

void find_substrings (String strings[], int amount_of_strings, String substrings[], int *amount_of_substrings, int *numbers_of_strings_with_substrings);
int find_first_shortest (String substrings[], int amount_of_substrings);
void delete_rus_symbols (String);
char string_is_not_empty (String);

int main() {
    setlocale(LC_ALL, "RUS"); system("chcp 1251"); system("cls");
    String strings[maximums::STRINGS], substrings[maximums::SUBSTRINGS];
    int n = 0, sub_n, str_numb[maximums::SUBSTRINGS];

    printf("Max length of sting = %d symbol. Input will be stopped after %d strings will be entered or after empty string.\nEnter strings:\n", maximums::LENGTH - 1, maximums::STRINGS);
    while (n < maximums::STRINGS && (string_is_not_empty(strings[n]) || n != 0) && (n == 0 || string_is_not_empty(strings[n - 1]))) {
        String tmp = "";

        fgets(strings[n], maximums::LENGTH, stdin);
        if (strings[n][strlen(strings[n]) - 1] == '\n')
            strings[n][strlen(strings[n]) - 1] = '\0';
        else
            fgets(tmp, maximums::LENGTH, stdin);
        n += ((string_is_not_empty(strings[n]) || n != 0) && (n == 0 || string_is_not_empty(strings[n - 1]))) ? 1 : 0;

        if (tmp[0] != '\0')
            printf("WARNING: There were to much symbols in %d string so they were deleted.\n", n);
    }
    if (n != maximums::STRINGS)
        n--;

    if (n > 0) {
        printf("Strings:\n");
        for (int i = 0; i < n; i++)
            printf("%3d - \"%s\"\n", i + 1, strings[i]);

        find_substrings(strings, n, substrings, &sub_n, str_numb); //task1
        if (sub_n == 0)
            printf("There are no substrings.\n");
        else {
            int shortest = find_first_shortest(substrings, sub_n); //task2

            printf("Substrings:\n");
            for (int i = 0; i < sub_n; i++) {
                printf("%3d - \"%s\"", i + 1, substrings[i]);
                putchar('\n');
            }
            printf("The shortest substring is %d: \"%s\"\n", shortest + 1, substrings[shortest]);

            delete_rus_symbols(strings[str_numb[shortest]]); //task3
            if (string_is_not_empty(strings[str_numb[shortest]]))
                printf("String number %d was transformed: \"%s\"\n", str_numb[shortest] + 1, strings[str_numb[shortest]]);
            else
                printf("String number %d was deleted.\n", str_numb[shortest] + 1);
        }
    } else
        printf("There are no strings.\n");

    system("pause");
    return 0;
}

void find_substrings (String str[], int n, String sub_str[], int *sub_n, int *str_numb) {
    *sub_n = 0;

    for (int i = 0; i < n; i++) {
        char *str_pointer, *begin_pointer;
        str_pointer = begin_pointer = str[i];

        while (*str_pointer != '\0') {
            if (*str_pointer < 0) {
                if (str_pointer != begin_pointer)
                    SUBSTRING
                while (*str_pointer != '\0' && *str_pointer < 0)
                    str_pointer++;
                begin_pointer = str_pointer;
            } else
                str_pointer++;
            if (*str_pointer == '\0')
                SUBSTRING
        }
    }
}
int find_first_shortest (String substr[], int sub_n) {
    int number_of_shortest = -1, shortest_length = maximums::LENGTH;

    for (int i = 0; i < sub_n; i++)
        if (strlen(substr[i]) < shortest_length) {
            shortest_length = strlen(substr[i]);
            number_of_shortest = i;
        }

    return number_of_shortest;
}
void delete_rus_symbols (String str) {
    String new_str = "";
    char *pointer = new_str;

    for (int i = 0; i < strlen(str); i++)
        if (str[i] < 0) {
            *pointer = str[i];
            pointer++;
        }
    *pointer = '\0';

    strncpy(str, new_str, strlen(new_str));
    str[strlen(new_str)] = '\0';
}
char string_is_not_empty (String string) {
    char *pointer = string;

    while (isspace(*pointer) || *pointer == '\n' || *pointer == '\0') {
        if (*pointer == '\n' || *(pointer + 1) == '\0' || *pointer == '\0')
            return 0;
        pointer++;
    }

    return 1;
}