#include <stdio.h>
#include <stdlib.h>

int create_input(void);
int create_main(void);

int main (int argc, char *argv[]) {
    if (create_input())
        return -1;
    if (create_main())
        return -1;

    system("g++ main.cpp -o second");
    system("cls");
    system("second < in.txt > out.txt");

    system("pause");
    return 0;
}

#define CHECK_FOR_OPENING(a) { if ((a) == NULL) {fprintf(stderr, "Can't create the file!\n"); return 1;} }

int create_input(void) {
    FILE *file_for_input = fopen("in.txt", "w");

    CHECK_FOR_OPENING(file_for_input)

    fprintf(file_for_input, "9\n\n"
            "Test Driven Development for Embedded C (Pragmatic Programmers)\n"
            "28\n352\n"
            "James W. Grenning\n2011\n"
            "The Pragmatic Bookshelf\nUSA\n\n"
            "Making Embedded Systems: Design Patterns for Great Software\n"
            "32\n330\n"
            "Elecia White\n2011\n"
            "O'Reilly Media\nUSA\n\n"
            "Expert C Programming: Deep C Secrets\n"
            "31\n384\n"
            "Peter van der Linden\n1994\n"
            "Prentice Hall\nUSA\n\n"
            "Practical C Programming\n"
            "26\n456\n"
            "Steve Oualline\n1997\n"
            "O'Reilly Media\nUSA\n\n"
            "sed & awk\n"
            "10\n434\n"
            "Dale Dougherty, Arnold Robbins\n1997\n"
            "O'Reilly Media\nUSA\n\n"
            "Perl Cookbook, Second Edition\n"
            "37\n968\n"
            "Tom Christiansen, Nathan Torkington\n2003\n"
            "O'Reilly Media\nUSA\n\n"
            "Learning the bash Shell\n"
            "24\n333\n"
            "Cameron Newham, Bill Rosenblatt\n2005\n"
            "O'Reilly Media\nUSA\n\n"
            "Learning Python\n"
            "49\n1214\n"
            "Mark Lutz, David Ascher\n2003\n"
            "O'Reilly Media\nUSA\n\n"
            "lsdkfjslkfjaj-oakwpfn aojfi aj papkf naoefpsfpk ankfj fk slag ojwifj p pke jfkosjefo\n"
            "124knl\nksdfjk\n23\n"
            "lakjf jpwoelksj oisj pej ofwj oje jwe pjwe fjweof woe jfpwjfp wjfokwncoksdofj wpe nokseno\n"
            "92419\n2142\n2000\n"
            "lsfjslkfjlskjflsk lksjflksjlkjfleskjflsdkjlfkjelifjsl sfje osejf sejf selk selk lksej lksej\n"
            "slekfjslekf slefjls\n\n");

    fclose(file_for_input);

    return 0;
}
int create_main(void) {
    FILE *file_main_cpp = fopen("main.cpp", "w");

    CHECK_FOR_OPENING(file_main_cpp)

    fprintf(file_main_cpp, "#include <stdio.h>\n#include <stdlib.h>\n#include <string.h>\n\n"
            "#define INTEGER(a, b) { fscanf(stdin, \"%s\", tmp); while ((b)) fscanf(stdin, \"%s\", tmp); (a) = (unsigned) atoi(tmp);}\n"
            "#define SCANF(a) {fgets((a), LENGTH, stdin); while ((a)[0] == '\\n' || (a)[0] == '\\0') fgets((a), LENGTH, stdin); \\\n"
            "if ((a)[strlen((a)) - 1] == '\\n') (a)[strlen((a)) - 1] = '\\0'; else fgets(extra, LENGTH, stdin); }\n", "%80s", "%80s");
    fprintf(file_main_cpp, "#define AUTHORS { String *author_pointer; for (author_pointer = ca_pointer->authors; author_pointer < ca_pointer->authors\\\n"
            "+ ca_pointer->amount_of_authors && strcmp(ca_pointer->authors[author_pointer - ca_pointer->authors], books_pointer->author\\\n"
            "); author_pointer++); if (author_pointer == ca_pointer->authors + ca_pointer->amount_of_authors) {strcpy(ca_pointer->authors\\\n"
            "[author_pointer - ca_pointer->authors], books_pointer->author); ca_pointer->amount_of_authors++; if (ca_pointer->\\\n"
            "amount_of_authors > max_amount_of_authors) max_amount_of_authors = ca_pointer->amount_of_authors;}}");
    fprintf(file_main_cpp, "\nenum maximums { BOOKS_AMOUNT = 10, LENGTH = 81 };\n\ntypedef char String[LENGTH];\ntypedef struct {\n\tString name, author;\n"
            "\tunsigned int price, pages;\n\tstruct {\n\t\tString name, city;\n\t\tunsigned int year;\n\t} publisher;\n} Book;\n\n"
            "Book books[BOOKS_AMOUNT];\nunsigned int n;\n\nvoid input();\nvoid output(void);\nvoid find_cities_with_max_authors(void);\n\n"
            "int main() {\n\tinput();\n\toutput();\n\tfind_cities_with_max_authors(); //does't need any checks\n\n\treturn 0;\n}\n");
    fprintf(file_main_cpp, "void input(void) {\n\tString tmp = \"\";\n\n\tINTEGER(n, atoi(tmp) <= 0 || atoi(tmp) > BOOKS_AMOUNT)\n\n"
            "\tfor (Book *books_pointer = books; books_pointer < books + n; books_pointer++) {\n\t\tString extra = \"\";\n\n"
            "\t\tSCANF(books_pointer->name)\n\t\tINTEGER(books_pointer->price, atoi(tmp) <= 0)\n\t\tINTEGER(books_pointer->pages, atoi(tmp) <= 0)\n");
    fprintf(file_main_cpp, "\t\tSCANF(books_pointer->author)\n\t\tINTEGER(books_pointer->publisher.year, atoi(tmp) <= 1800 || atoi(tmp) > 2016)\n"
            "\t\tSCANF(books_pointer->publisher.name)\n\t\tSCANF(books_pointer->publisher.city)\n\n\t\tif (*extra != '\\0')\n"
            "\t\t\tfprintf(stderr, \"WARNING: There was too mush information in some fields so it was deleted.\\n\");\n\t}\n}\n");
    fprintf(file_main_cpp, "void output(void) {\n\tfprintf(stdout, \"\\t%s\\n\", (n == 1) ? \"Entered book:\" : \"List of entered books:\");\n"
            "\t\tfor (Book *books_pointer = books; books_pointer < books + n; books_pointer++) {\n\t\tfprintf(stdout, \"\\n  %s Book:\\nName: "
            "\\\"%s\\\"\\nPrice: \\\"%s\\\"\\n\", books_pointer - books + 1, books_pointer->name, books_pointer->price);\n", "%s", "%lu", "%s", "%u");
    fprintf(file_main_cpp, "\t\tfprintf(stdout, \"Number of pages: \\\"%s\\\"\\nAuthor(s): \\\"%s\\\"\\n\", books_pointer->pages, books_pointer->author);\n"
            "\t\tfprintf(stdout, \"Year: \\\"%s\\\"\\nPublisher: \\\"%s\\\"\\n\", books_pointer->publisher.year, books_pointer->publisher.name);\n"
            "\t\tfprintf(stdout, \"City: \\\"%s\\\"\\n\", books_pointer->publisher.city);\n}\n}\n", "%u", "%s", "%u", "%s", "%s");
    fprintf(file_main_cpp, "void find_cities_with_max_authors(void) {\n\tstruct {\n\t\tString city = \"\", authors[BOOKS_AMOUNT];\n"
            "\t\tint amount_of_authors = 0;\n\t} city_authors[BOOKS_AMOUNT], *ca_pointer;\n\tint amount_of_structures = 0, max_amount_of_authors = 0;\n\n"
            "\tfor (Book *books_pointer = books; books_pointer < books + n; books_pointer++) {\n");
    fprintf(file_main_cpp, "\tfor (ca_pointer = city_authors; ca_pointer < city_authors + amount_of_structures && strcmp(ca_pointer->city, books_pointer->"
            "publisher.city); ca_pointer++);\n\n\t\t\tif (ca_pointer == city_authors + amount_of_structures) {\n\t\t\tstrcpy(ca_pointer->city, books_"
            "pointer->publisher.city);\n\t\t\tAUTHORS\n\t\t\tamount_of_structures++;\n\t\t} else\n\t\t\tAUTHORS\n}\n\n");
    fprintf(file_main_cpp, "\tfprintf(stdout, \"\\n\\tCit%s with maximum amount of authors (%s):\\n\", (amount_of_structures == 1) ? \"y\" : \"ies\", max_amount_of_authors);\n"
            "\tfor (ca_pointer = city_authors; ca_pointer < city_authors + amount_of_structures; ca_pointer++)\n\t\tif (ca_pointer->amount_of_authors "
            "== max_amount_of_authors)\n\t\t\tfprintf(stdout, \"\\\"%s\\\"\\n\", ca_pointer->city);\n}", "%s", "%d", "%s");

    fclose(file_main_cpp);

    return 0;
}