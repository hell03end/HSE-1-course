#include <stdio.h>
#include <iostream>
#include <stdlib.h>

void input (int *, int *);

int main () {
  system("chcp 1251");
  system("cls");
  setlocale(LC_ALL, "RUS");
  int i, from, to;

  input(&from, &to);

  for (i = from; i < to; i += 5) {
    printf("%3d - %c\t\t\t%3d - %c\t\t\t%3d - %c\t\t\t%3d - %c\t\t\t%3d - %c\t\t\t\n", i, i, i + 1, i + 1, i + 2, i + 2, i + 3, i + 3, i + 4, i + 4);
  }

  system("pause");
  return 0;
}

void input (int *from, int *to) {
	printf("Enter low border (from): ");
  	scanf("%3d", from);
  	printf("Enter higher border (to): ");
  	scanf("%3d", to);
  	system("cls");
}