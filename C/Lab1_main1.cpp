//A, B int arrays; cr C wth odd nmbrs (>1 in B) && !(not)in A.
//	&& - there is no chck for lttrs && R-nmbrs.
//	??(0;0) !! (0;0) &&(1;0)

#include <stdio.h>
#include <stdlib.h>

#define BRDR 25

void inpt (char, int *, int *);
void otpt (int *, int);

int main(int argc, char *argv[]) {
	int a[BRDR], b[BRDR], c[BRDR];
	int lnA, lnB, lnC, i;

	inpt((char)'A', a, &lnA);
	otpt(a, lnA);

	inpt((char)'B', b, &lnB);
	otpt(b, lnB);

	lnC = 0;
	for (i = 0; i < lnB; i += 2) {
		int j;

		for (j = 0; j < lnB && (*(b + i) != *(b + j) || i == j); j++)
			;
		if (j < lnB) {
			for (j = 0; j < lnA && *(b + i) != *(a + j); j++)
				;
			if (j == lnA) {
				for (j = 0; j < lnC && *(b + i) != *(c + j); j++)
					;
				if (j == lnC)
					*(c + lnC++) = *(b + i);
			}
		}
	}
	if (lnC == 0)
		printf("<<\tERROR: there are no elmnts in C.\n");
	else
		otpt(c, lnC);

	return 0;
}

void inpt (char nm, int *ar, int *ln) {
	int i;

	printf(">>\tenter length of %c:\t", nm);
	scanf("%d", ln);
	while (*ln <= 0 || *ln > BRDR) //should be N
		scanf("%d", ln);
	printf(">>\tenter %c:\n", nm);
	for (i = 0; i < *ln; i++)
		scanf("%d", ar + i);

	return;
}

void otpt (int *ar, int ln) {
	int i;

	for (i = 0; i < ln; i++)
		printf("%10d", *(ar + i));
	putchar('\n');

	return;
}
