// int a[][]; swtch min & max
//	&& - there is no chck for lttrs && R-nmbrs.
//	??(0;0) !! (0;0) &&(1;0)

#include <stdio.h> 
#include <stdlib.h>

#define BRDR 25

void inpt (char, int *, int);
void otpt (int (*)[2*BRDR], int, int);
int fnd (int (*)[2 * BRDR], int, int, char);

int main(int argc, char *argv[]) {
	int a[BRDR][2 * BRDR];
	int x, y, i;
	int min, max;

	inpt((char)'x', &y, 2 * BRDR);
	inpt((char)'y', &x, BRDR);
	
	printf(">>\tenter A:\n");
	for (i = 0; i < x; i++) {
		int j;

		for (j = 0; j < y; j++)
			scanf("%d", &a[i][j]);
	}
	otpt(a, x, y);

	min = fnd(a, x, y, (char)'<');
	max = fnd(a, x, y, (char)'>');

	if (min != max) {
		putchar('\n');
		for (i = 0; i < x; i++) {
			int j;

			for (j = 0; j < y; j++)
				if (a[i][j] == max)
					a[i][j] = min;
				else if (a[i][j] == min)
					a[i][j] = max;
		}
		otpt(a, x, y);
	} else
		printf("<<ERROR\tmin = max, there were no changes.\n");

	return 0;
}

void inpt (char nm, int *ln, int brd) {
	printf(">>\tenter length of %c:\t", nm);
	scanf("%d", ln);
	while (*ln <= 0 || *ln > brd) //should be N
		scanf("%d", ln);

	return;
}

void otpt (int (*ar)[2*BRDR], int x, int y) {
	int i;

	for (i = 0; i < x; i++) {
		int j;

		putchar('\t');
		for (j = 0; j < y; j++)
			printf("%d\t", ar[i][j]);
		putchar('\n');
	}

	return;
}

int fnd (int (*ar)[2 * BRDR], int x, int y, char key) {
	int tmp, i;

	tmp = ar[0][0];
	for (i = 0; i < x; i++) {
		int j;

		for (j = 0; j < y; j++)
			if (key == '<' && ar[i][j] < tmp)
				tmp = ar[i][j];
			else if (key == '>' && ar[i][j] > tmp)
				tmp = ar[i][j];
	}

	return tmp;
}