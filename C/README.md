# С works
Данный раздел содержит выполнение заданий по курсу «Информатика и программирование 1 курс (code 65369)» (НИУ ВШЭ, 2015-16). Содержимое раздела распространяется на условиях лицензии [MIT License](https://github.com/hell03end/HSE-1-course/blob/master/LICENSE). При использовании материалов обязательно упоминание автора работ. При наличии технической возможности необходимо также указать активную гиперссылку на [репозиторий автора](https://github.com/hell03end/) или, собственно, на данный раздел.

*I use `.cpp` instead of `.c` extensions 'cause it was required.*

Tasks can be found in "**`src/`**".

**Made by [hell03end](https://github.com/hell03end/).**

2015-16.
