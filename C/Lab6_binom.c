#include <stdio.h>
#include <stdlib.h>

#ifndef INT_MAX
#define INT_MAX		2147483647
#endif

enum maximums { BORDER = 50, LENGTH = 11 };

unsigned int n, m;

unsigned int get_natural_integer (unsigned int low_border, unsigned int high_border);
int coutn (int n, int m);

int main() {
    printf("n (n > 0):\n");
    n = get_natural_integer(1, BORDER);
    printf("m (m > 0, m < n):\n");
    m = get_natural_integer(0, n);

    printf("Result is:\t%u\n", coutn(n, m));

    system("pause");
    return 0;
}

unsigned int get_natural_integer (unsigned int low_border, unsigned int high_border) {
    char tmp[LENGTH] = "";

    printf("Enter number from %d up to %d:\t", low_border + 1, high_border - 1);
    scanf("%s", tmp);
    while (!atoi(tmp) || atoi(tmp) > INT_MAX || atoi(tmp) >= high_border || atoi(tmp) <= low_border) {
        system("cls");
        printf("Enter again, please (from %d up to %d):\t", low_border + 1, high_border - 1);
        scanf("%s", tmp);
    }

    system("cls");
    return (unsigned int) atoi(tmp);
}
int coutn (int n, int m) {
    return (m == 0 || m == n) ? 1 : coutn(n - 1, m) + coutn(n - 1, m - 1);
}