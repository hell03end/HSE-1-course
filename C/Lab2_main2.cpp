// int a[][]; swtch min & max
//	&& - there is no chck for lttrs && R-nmbrs.
//	??(0;0) !! (0;0) &&(1;0)

#include <iostream>

using namespace std;

#define BRDR 25

void inpt (char, int *, int);
void inpar (int (*)[2 * BRDR], int, int);
void otpt (int (*)[2 * BRDR], int, int);
int fnd (int (*)[2 * BRDR], int, int, char);
void swtch (int (*)[2 * BRDR], int, int, int, int);

int main(int argc, char *argv[]) {
	int a[BRDR][2 * BRDR];
	int x, y;
	int min, max;

	inpt((char)'x', &y, 2 * BRDR);
	inpt((char)'y', &x, BRDR);
	
 	inpar (a, x, y);
	otpt(a, x, y);

	min = fnd(a, x, y, (char)'<');
	max = fnd(a, x, y, (char)'>');

	if (min != max) {
		putchar('\n');
		swtch(a, x, y, min, max);
		otpt(a, x, y);
	} else
		printf("ERROR\tmin = max, there were no changes.\n");

	return 0;
}

void inpt (char nm, int *ln, int brd) {
	cout << "\tenter length of " << nm << ":\t";
	cin >> *ln;
	while (*ln <= 0 || *ln > brd) //should be N
		cin >> *ln;

	return;
}

void inpar (int (*ar)[2 * BRDR], int x, int y) {
	int i;

	cout << "\tenter A:\n";
	for (i = 0; i < x; i++) {
		int *pntr;

		for (pntr = ar[i]; pntr < ar[i] + y; pntr++)
			cin >> *pntr;
	}

	return;
}

void otpt (int (*ar)[2 * BRDR], int x, int y) {
	int i;

	for (i = 0; i < x; i++) {
		int *pntr;

		putchar('\t');
		for (pntr = ar[i]; pntr < ar[i] + y; pntr++)
			cout << *pntr << '\t';
		putchar('\n');
	}

	return;
}

int fnd (int (*ar)[2 * BRDR], int x, int y, char key) {
	int tmp, i;

	tmp = ar[0][0];
	for (i = 0; i < x; i++) {
		int *pntr;

		for (pntr = ar[i]; pntr < ar[i] + y; pntr++)
			if (key == '<' && *pntr < tmp)
				tmp = *pntr;
			else if (key == '>' && *pntr > tmp)
				tmp = *pntr;
	}

	return tmp;
}

void swtch (int (*ar)[2 * BRDR], int x, int y, int min, int max) {
	int i;

	for (i = 0; i < x; i++) {
		int *pntr;

		for (pntr = ar[i]; pntr < ar[i] + y; pntr++)
			if (*pntr == max)
				*pntr = min;
			else if (*pntr == min)
				*pntr = max;
	}

	return;
}