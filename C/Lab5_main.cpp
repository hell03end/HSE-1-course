#include <time.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void *__gxx_personality_v0;

#define GET_INFO(a) { String marks = ""; \
fscanf((a), "%s", element->student.initials.surname); \
fscanf((a), "%s", element->student.initials.name); \
fscanf((a), "%s", element->student.initials.patronymic); \
fscanf((a), "%s", element->student.group); \
fgetc((a)); fgets(marks, LENGTH, (a)); \
for (char tmp[3] = "  ", j = 0, c, i = 0, n = 0; (c != '\n' && c != '\0') || n < MARKS; ) { \
    c = marks[j++]; \
    if (isdigit(c)) tmp[i++] = c; \
    else if (!isdigit(c) || n == MARKS - 1) { \
        if (isdigit(tmp[0]) && atoi(tmp) < 11) element->student.progress.marks[n++] = (unsigned short) atoi(tmp); \
        i = 0; tmp[1] = ' '; } } \
for (char i = 0; i < MARKS; i++) element->student.progress.middle_mark += element->student.progress.marks[i]; \
element->student.progress.middle_mark /= (float) MARKS; }
#define RETURN_ERROR(a, b) { fprintf(stderr, (a)); system("pause"); return (b); }

enum maximums { LENGTH = 81, MARKS = 5 };

typedef char String[LENGTH];
typedef struct {
    struct {
        String name = "", surname = "", patronymic = "";
    } initials;
    String group = ""; //can it be just a number instead?
    struct {
        unsigned short marks[MARKS]; // 0 - 10
        float middle_mark = 0; // calculate while reading
    } progress;
} Student;
typedef struct _StackElement {
    Student student;
    _StackElement *next = NULL, *previous = NULL;
} StackElement;
typedef struct {
    size_t size = 0;
    StackElement *first_element = NULL, *last_element = NULL;
} Stack;

Stack *stack = new Stack;
FILE *input_file;

char open_file(void);
int input(char number_of_arguments);
void output(void);
char add_new_structure(char number_of_arguments);
void delete_list(void);

int main(int argc, char *argv[]) {
    for (int argument; (argument = getopt(argc, argv, "f")) != EOF; )
		if (argument == 'f')
			if (!open_file()) 
                RETURN_ERROR("ERROR: Can't create file for output!\n", 1)

    if (input(argc) == -1) 
        RETURN_ERROR("ERROR: file is empty!\n", 2)
    if (!add_new_structure(argc)) 
        RETURN_ERROR("ERROR: Can't find such student or group!\n", 3)
    output();

    if (argc == 1)
        system("pause");
    delete_list();
    return 0;
}

char open_file(void) {
    String tmp_in = "", file_name = "";
    time_t rawtime = time (NULL);
    struct tm * timeinfo = localtime (&rawtime);

    fprintf(stdout, "Enter name to file for input: ");
    fscanf(stdin, "%s", tmp_in);
    while (!(input_file = fopen(tmp_in, "r"))) {
        system("cls");
        fprintf(stderr, "ERROR: Can't open this file.\nEnter again, please: ");
        fscanf(stdin, "%s", tmp_in);
    }
    system("cls");

    strftime (file_name, LENGTH, "%I.%M.%S %d-%m-%Y", timeinfo);
    strcat(file_name, ".out.txt");

    return (freopen(file_name, "w", stdout)) ? 1 : 0;
}
int input(char number_of_arguments) {
    if (number_of_arguments != 1) {
        fseek(input_file, 0, SEEK_END);
        if (ftell(input_file) == 0)
            return -1;
        rewind(input_file);
    }

    for (int end_of_file = '\n'; end_of_file != EOF; ) {
        StackElement *element = new StackElement;

        stack->size++;
        if (stack->first_element == NULL)
            stack->first_element = element;
        else {
            element->previous = stack->last_element;
            stack->last_element->next = element;
        }
        stack->last_element = element;

        if (number_of_arguments != 1)
            GET_INFO(input_file)
        else {
            fprintf(stderr, "Enter information about students.\n(surname, name, patronymic, group, marks)\n");
            GET_INFO(stdin)
        }

        if (number_of_arguments != 1)
            end_of_file = fgetc(input_file);
        else {
            system("cls");
            fprintf(stderr, "Do you want to continue? [Y/n]\n");
            fscanf(stdin, "%c", &end_of_file);
            if (end_of_file != '\n')
                getchar();
            while (end_of_file != 'N' && end_of_file != 'n' && end_of_file != 'Y' && end_of_file != 'y') {
                system("cls");
                fprintf(stderr, "Enter again, please [Y/n]:\t");
                fscanf(stdin, "%c", &end_of_file); \
                if (end_of_file != '\n')
                    getchar();
            }
            end_of_file = (end_of_file == 'N' || end_of_file == 'n') ? EOF : '\n';
            system("cls");
        }
    }

    if (number_of_arguments != 1)
        fclose(input_file);
    return 0;
}
void output(void) {
    for (StackElement *element = stack->last_element; element; element = element->previous) {
        fprintf(stdout, "%s\n", element->student.initials.surname);
        fprintf(stdout, "%s\n", element->student.initials.name);
        fprintf(stdout, "%s\n", element->student.initials.patronymic);
        fprintf(stdout, "%s\n", element->student.group);
        for (char i = 0; i < MARKS; i++)
            fprintf(stdout, "%3d", element->student.progress.marks[i]);
        fprintf(stdout, "\n%.1f\n\n", element->student.progress.middle_mark);
    }
}
char add_new_structure(char number_of_arguments) {
    String name = "", surname = "", patronymic = "", group = "";
    int n = stack->size;

    fprintf(stderr, "Enter surname of the student:\t");
    fscanf(stdin, "%s", surname);
    fprintf(stderr, "Enter name of the student:\t");
    fscanf(stdin, "%s", name);
    fprintf(stderr, "Enter patronymic of the student:\t");
    fscanf(stdin, "%s", patronymic);
    fprintf(stderr, "Enter group of the student:\t");
    fscanf(stdin, "%s", group);
    if (number_of_arguments == 1)
        system("cls");

    for (StackElement *pointer = stack->last_element; pointer; pointer = pointer->previous) {
        if (!strcmp(pointer->student.initials.name, name) && !strcmp(pointer->student.initials.surname, surname) && !strcmp(pointer->student.initials.patronymic, patronymic) && !strcmp(pointer->student.group, group)) {
            StackElement *element = new StackElement;

            fprintf(stderr, "Now, enter information to add.\n(surname, name, patronymic, group, marks)\n");
            GET_INFO(stdin)
            if (number_of_arguments == 1)
                system("cls");

            stack->size++;
            element->next = pointer;
            element->previous = pointer->previous;
            if (pointer->previous)
                pointer->previous->next = element;
            pointer->previous = element;
            pointer = element;
        }
    }

    return (stack->size == n) ? 0 : 1;
}
void delete_list(void) {
    for (StackElement *tmp = stack->last_element, *next = NULL; tmp; tmp = next) {
        next = tmp->previous;
        delete tmp;
    }

    stack->size = 0;
    delete stack;
    stack = NULL;
}