//A, B int arrays; cr C wth odd nmbrs (>1 in B) && !(not)in A.
//	&& - there is no chck for lttrs && R-nmbrs.
//	??(0;0) !! (0;0) &&(1;<...>)

#include <iostream>

using namespace std; 

#define BRDR 25

void inpt (char, int *, int *);
void otpt (int *, int);

int main(int argc, char *argv[]) {
	int a[BRDR], b[BRDR], c[BRDR];
	int lnA, lnB, lnC;
	int *pntr;

	inpt((char)'A', a, &lnA);
	otpt(a, lnA);

	inpt((char)'B', b, &lnB);
	otpt(b, lnB);

	lnC = 0;
	for (pntr = b; pntr < b + lnB; pntr += 2) {
		int *pj;

		for (pj = b; pj < b + lnB && (*pntr != *pj || pntr == pj); pj++)
			;
		if (pj < b + lnB) {
			for (pj = a; pj < a + lnA && *pntr != *pj; pj++)
				;
			if (pj == a + lnA) {
				for (pj = c; pj < c + lnC && *pntr != *pj; pj++)
					;
				if (pj == c + lnC) {
					lnC++;
					*pj = *pntr;
				}
			}
		}
	}
	if (lnC == 0)
		cout << "\tERROR: there are no elmnts in C.\n";
	else
		otpt(c, lnC);

	return 0;
}

void inpt (char nm, int *ar, int *ln) {
	int *pntr;

	cout << "\tenter length of " << nm << ":\t";
	cin >> *ln;
	while (*ln <= 0 || *ln > BRDR) //should be N
		cin >> *ln;
	cout << "\tenter " << nm << ":\n";
	for (pntr = ar; pntr < ar + *ln; pntr++)
		cin >> *pntr;

	return;
}

void otpt (int *ar, int ln) {
	int *pntr;

	putchar('\t');
	for (pntr = ar; pntr < ar + ln; pntr++)
		cout << *pntr << "\t";
	putchar('\n');

	return;
}