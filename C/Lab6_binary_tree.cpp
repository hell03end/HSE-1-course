#include <stdio.h>
#include <stdlib.h>

void *__gxx_personality_v0;

#define GET_NEW_LINE { if (want_to_continue != '\n') getchar(); }
#define PRINT_TREE { if (tree->number_of_nodes == 0) fprintf(stderr, "There are no elements in tree!\n"); \
else print_tree(tree->root, START); system("pause"); system("cls"); }
#ifndef INT_MAX
#define INT_MAX		2147483647
#endif

enum maximums { START, LENGTH = 11, BRANCHES = 100 };

typedef struct _TreeNode{
    int data;
    _TreeNode *left, *right;
} TreeNode;
typedef struct {
    TreeNode *root;
    int number_of_nodes;
} Tree;

Tree *tree = (Tree *) malloc(sizeof(Tree));

int get_integer (int low_border, int high_border);
TreeNode * add_to_tree(TreeNode *root, TreeNode *t, int info);
void print_tree(TreeNode *root, int iteration);
TreeNode * delete_leaves_from_tree (TreeNode *root);

int main() {
    tree->root = NULL;
    tree->number_of_nodes = 0;

    for (char want_to_continue = 'Y'; (want_to_continue == 'Y' || want_to_continue == 'y') && tree->number_of_nodes < BRANCHES; system("cls")) {
        int value = get_integer(-INT_MAX, INT_MAX);

        tree->root = add_to_tree(tree->root, tree->root, value);

        system("cls");
        GET_NEW_LINE
        fprintf(stderr, "Do you want to continue? [Y/n]\n");
        fscanf(stdin, "%c", &want_to_continue);
        GET_NEW_LINE
        while (want_to_continue != 'N' && want_to_continue != 'n' && want_to_continue != 'Y' && want_to_continue != 'y') {
            system("cls");
            fprintf(stderr, "Enter again, please [Y/n]:\t");
            fscanf(stdin, "%c", &want_to_continue);
            GET_NEW_LINE
        }
    }

    PRINT_TREE
    tree->root = delete_leaves_from_tree(tree->root);
    PRINT_TREE

    return 0;
}

int get_integer (int low_border, int high_border) {
    char tmp[LENGTH] = "";

    printf("Enter number from %d up to %d:\t", low_border + 1, high_border - 1);
    scanf("%s", tmp);
    while ((!atoi(tmp) && tmp[0] != '0') || abs(atoi(tmp)) >= INT_MAX || atoi(tmp) <= low_border || atoi(tmp) >= high_border) {
        system("cls");
        printf("Enter again, please (from %d up to %d):\t", low_border + 1, high_border - 1);
        scanf("%s", tmp);
    }

    system("cls");
    return atoi(tmp);
}
TreeNode * add_to_tree(TreeNode *root, TreeNode *tree_node, int value) {
    if (!tree_node) {
        tree_node = (TreeNode *) malloc(sizeof(TreeNode));
        if (!tree_node) {
            printf("There is no memory!\n");
            system("pause");
            exit(1);
        }
        tree->number_of_nodes++;
        tree_node->left = tree_node->right = NULL;
        tree_node->data = value;
        if (!root)
            return tree_node;
        if (value < root->data)
            root->left = tree_node;
        else if (value > root->data)
            root->right = tree_node;

        return root;
    }

    if (value != tree_node->data)
        add_to_tree(tree_node, (value < tree_node->data) ? tree_node->left : tree_node->right, value);

    return root;
}
void print_tree(TreeNode *root, int iteration) {
    if (root == NULL)
        return;

    print_tree(root->right, iteration + 1);
    for (int i = 0; i < iteration; i++)
        putchar('\t');
    printf("%d\n", root->data);
    print_tree(root->left, iteration + 1);
}
TreeNode * delete_leaves_from_tree (TreeNode *root) {
    if (!root)
        return root;

    if (root->right == root->left && root->left == NULL) {
        tree->number_of_nodes--;
        free(root);
        return NULL;
    }

    root->left = delete_leaves_from_tree(root->left);
    root->right = delete_leaves_from_tree(root->right);

    return root;
}