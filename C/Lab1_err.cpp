//A, B int arrays; cr C wth odd nmbrs (>1 in B) && !in A.
//	&& - in "lng" there is no chck for lttrs && R-nmbrs.
//	??(0;0) !! (?;0) &&(1;<...>)

#include <stdlib.h>
#include <stdio.h>

void lng (int, int **, int *);
void inpAr (int, int *, int);
void otpt (int *, int);
void crC (int *, int, int *, int, int **, int);

int main() {
	int lnA, lnB, lnC; //arrays lengths
	int *a, *b, *c; //arrays
 //A:
	lng('A', &a, &lnA);
	inpAr('A', a, lnA); //inpt of array A
	otpt(a, lnA);
 //B:
	lng('B', &b, &lnB);
	inpAr('B', b, lnB); //inpt of array B
	otpt(b, lnB);
 //C:
	lnC = 0;
	crC(a, lnA, b, lnB, &c, lnC);
	if (lnC > 0)
		otpt(c, lnC);
	else
		printf("There is no elements.\n");
 //return memory->
	free(a);
	free(b);
	free(c);
 //end->
	getchar(); //press anything
	getchar();
	return 0;
}

void lng (int nm, int **ar, int *ln) {
	printf(">> enter length for %c:\t", nm);
	scanf("%d", ln);
	while (*ln <= 0 && *ln > 100) //should be N
		scanf("%d", ln);
	*ar = (int *) malloc(*ln * sizeof(int));

	return;
}

void inpAr (int nm, int *ar, int ln) {
	int i;

	printf(">> enter %c:\n", nm); 
	for (i = 0; i < ln; i++) {
		printf("%c[%2d] = ", nm, i);
		scanf("%d", ar + i);
		//chck for lttrs and R-nmbrs
	}

	return;
}

void otpt (int *ar, int ln) {
	int i;

	for (i = 0; i < ln; i++)
		printf("%10d", *(ar + i));
	putchar('\n');

	return;
}

void crC (int *a, int lnA, int *b, int lnB, int **c, int lnC) {
	int i;

	for (i = 0; i < lnB; i += 2) { //only even elmnts of b[]
		int j;

		for (j = 0; j < lnB && (*(b + i) != *(b + j) || i == j); j++)
			printf(" %d-%d\t(%d != %d)\n\r", i, j, *(b + i), *(b + j))
			;
		if (j < lnB) { //there are same even elmnts in b[]
			for (j = 0; j < lnA && *(b + i) != *(a + j); j++)
				printf("\t%d [%d]\t(%d, %d)\n\r", i, j, *(b + i), *(a + j))
				;
			if (j == lnA) { //there are no b[i] in a[]
				printf("%s\n\r", "OK.");
				if (lnC == 0) {
					*c = (int *) malloc(sizeof(int));
					**(c + lnC++) = *(b + i);
					printf("<<%d: %d == %d\n\r", lnC, **(c + lnC - 1), *(b + i));
				} else {
					for (j = 0; j < lnC && *(b + i) != **(c + j); j++)
						printf("\t\t%d [%d]\t(%d, %d)\n\r", i, j, *(b + i), **(c + j))
						;
					if (j == lnC) { //there are no b[i] in c[]
						*c = (int*) realloc(*c, (lnC + 1) * sizeof(int));
						**(c + lnC++) = *(b + i);
						printf("<<%d: %d == %d\n\r", lnC, **(c + lnC - 1), *(b + i));
					}
				}
			}
		}
	}

	return;
}
