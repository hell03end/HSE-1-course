#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INTEGER(a, b) { scanf("%80s", tmp); while ((b)) { printf("Enter again, please: "); \
scanf("%80s", tmp); } (a) = (unsigned) atoi(tmp); }
#define SCANF(a) { fgets((a), LENGTH, stdin); while ((a)[0] == '\n' || (a)[0] == '\0') fgets((a), LENGTH, stdin); \
if ((a)[strlen((a)) - 1] == '\n') (a)[strlen((a)) - 1] = '\0'; else fgets(extra, LENGTH, stdin); }
#define AUTHORS { int k; for (k = 0; k < (city_authors + j)->amount_of_authors && strcmp((city_authors + j)->\
authors[k], (books + i)->author); k++); if (k == (city_authors + j)->amount_of_authors) {strcpy((city_authors + \
j)->authors[k], (books + i)->author); (city_authors + j)->amount_of_authors++; if ((city_authors + j)->\
amount_of_authors > max_amount_of_authors) max_amount_of_authors = (city_authors + j)->amount_of_authors;} };

enum maximums { BOOKS_AMOUNT = 10, LENGTH = 81 };

typedef char String[LENGTH];
typedef struct {
    String name, author;
    unsigned int price, pages;
    struct {
        String name, city;
        unsigned int year;
    } publisher;
} Book;

Book books[BOOKS_AMOUNT];
unsigned int n;

void input(void);
void output(void);
void find_cities_with_max_authors(void);

int main() {
    input();
    output();
    find_cities_with_max_authors(); //does't need any checks

    system("pause");
    return 0;
}

void input(void) {
    String tmp = "";

    printf("Enter the amount of books (maximum %d): ", BOOKS_AMOUNT);
    INTEGER(n, atoi(tmp) <= 0 || atoi(tmp) > BOOKS_AMOUNT)
    printf("Maximum length of any name is %d. ONLY ENGLISH LANGUAGE SUPPORTED!!!\nEnter information about book%s:\n",  LENGTH - 1, (n == 1) ? "" : "s");
    for (int i = 0; i < n; i++) {
        String extra = "";

        printf("\t%3d Book\n", i + 1);
        printf("Enter name of the book: ");
        SCANF((books + i)->name)
        printf("Enter price of the book: ");
        INTEGER((books + i)->price, atoi(tmp) <= 0)
        printf("Enter number of pages in book: ");
        INTEGER((books + i)->pages, atoi(tmp) <= 0)
        printf("Enter author of the book: ");
        SCANF((books + i)->author)
        printf("Enter year of publishing the book: ");
        INTEGER((books + i)->publisher.year, atoi(tmp) <= 1800 || atoi(tmp) > 2016)
        printf("Enter name of the publisher of the book: ");
        SCANF((books + i)->publisher.name)
        printf("Enter city of publishing the book: ");
        SCANF((books + i)->publisher.city)
        putchar('\n');

        if (*extra != '\0')
            fprintf(stderr, "WARNING: There was too mush information in some fields so it was deleted.\n");
    }
}
void output(void) {
    printf("\t%s\n", (n == 1) ? "Entered book:" : "List of entered books:");
    for (int i = 0; i < n; i++) {
        printf("  %3d Book:\nName: \"%s\"\nPrice: \"%u\"\n", i + 1, (books + i)->name, (books + i)->price);
        printf("Number of pages: \"%u\"\nAuthor(s): \"%s\"\n", (books + i)->pages, (books + i)->author);
        printf("Year: \"%u\"\nPublisher: \"%s\"\n", (books + i)->publisher.year, (books + i)->publisher.name);
        printf("City: \"%s\"\n", (books + i)->publisher.city);
    }
}
void find_cities_with_max_authors(void) {
    struct {
        String city = "", authors[BOOKS_AMOUNT];
        int amount_of_authors = 0;
    } city_authors[BOOKS_AMOUNT];
    int amount_of_structures = 0, max_amount_of_authors = 0;

    for (int i = 0; i < n; i++) {
        int j;

        for (j = 0; j < amount_of_structures && strcmp((city_authors + j)->city, (books + i)->publisher.city); j++)
            ; //try to find same cities

        if (j == amount_of_structures) { //there are no same cities - add new
            strcpy((city_authors + j)->city, (books + i)->publisher.city); //add new city in array
            AUTHORS
            amount_of_structures++;
        } else
            AUTHORS
    }

    printf("\n\tCit%s with maximum amount of authors (%d):\n", (amount_of_structures == 1) ? "y" : "ies", max_amount_of_authors);
    for (int i = 0; i < amount_of_structures; i++)
        if ((city_authors + i)->amount_of_authors == max_amount_of_authors)
            printf("\"%s\"\n", (city_authors + i)->city);
}