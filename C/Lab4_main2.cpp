#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INTEGER(a, b) { fscanf(stdin, "%80s", tmp); while ((b)) fscanf(stdin, "%80s", tmp); (a) = (unsigned) atoi(tmp);}
#define SCANF(a) {fgets((a), LENGTH, stdin); while ((a)[0] == '\n' || (a)[0] == '\0') fgets((a), LENGTH, stdin); \
if ((a)[strlen((a)) - 1] == '\n') (a)[strlen((a)) - 1] = '\0'; else fgets(extra, LENGTH, stdin); }
#define AUTHORS { String *author_pointer; for (author_pointer = ca_pointer->authors; author_pointer < ca_pointer->authors\
+ ca_pointer->amount_of_authors && strcmp(ca_pointer->authors[author_pointer - ca_pointer->authors], books_pointer->author\
); author_pointer++); if (author_pointer == ca_pointer->authors + ca_pointer->amount_of_authors) {strcpy(ca_pointer->authors\
[author_pointer - ca_pointer->authors], books_pointer->author); ca_pointer->amount_of_authors++; if (ca_pointer->\
amount_of_authors > max_amount_of_authors) max_amount_of_authors = ca_pointer->amount_of_authors;}}

enum maximums { BOOKS_AMOUNT = 10, LENGTH = 81 };

typedef char String[LENGTH];
typedef struct {
    String name, author;
    unsigned int price, pages;
    struct {
        String name, city;
        unsigned int year;
    } publisher;
} Book;

Book books[BOOKS_AMOUNT];
unsigned int n;

void input();
void output(void);
void find_cities_with_max_authors(void);

int main() {
    input();
    output();
    find_cities_with_max_authors(); //does't need any checks

    return 0;
}

void input(void) {
    String tmp = "";

    INTEGER(n, atoi(tmp) <= 0 || atoi(tmp) > BOOKS_AMOUNT)

    for (Book *books_pointer = books; books_pointer < books + n; books_pointer++) {
        String extra = "";

        SCANF(books_pointer->name)
        INTEGER(books_pointer->price, atoi(tmp) <= 0)
        INTEGER(books_pointer->pages, atoi(tmp) <= 0)
        SCANF(books_pointer->author)
        INTEGER(books_pointer->publisher.year, atoi(tmp) <= 1800 || atoi(tmp) > 2016)
        SCANF(books_pointer->publisher.name)
        SCANF(books_pointer->publisher.city)

        if (*extra != '\0')
            fprintf(stderr, "WARNING: There was too mush information in some fields so it was deleted.\n");
    }
}
void output(void) {
    fprintf(stdout, "\t%s\n", (n == 1) ? "Entered book:" : "List of entered books:");
    for (Book *books_pointer = books; books_pointer < books + n; books_pointer++) {
        fprintf(stdout, "\n  %3lu Book:\nName: \"%s\"\nPrice: \"%u\"\n", books_pointer - books + 1, books_pointer->name, books_pointer->price);
        fprintf(stdout, "Number of pages: \"%u\"\nAuthor(s): \"%s\"\n", books_pointer->pages, books_pointer->author);
        fprintf(stdout, "Year: \"%u\"\nPublisher: \"%s\"\n", books_pointer->publisher.year, books_pointer->publisher.name);
        fprintf(stdout, "City: \"%s\"\n", books_pointer->publisher.city);
    }
}
void find_cities_with_max_authors(void) {
    struct {
        String city = "", authors[BOOKS_AMOUNT];
        int amount_of_authors = 0;
    } city_authors[BOOKS_AMOUNT], *ca_pointer;
    int amount_of_structures = 0, max_amount_of_authors = 0;

    for (Book *books_pointer = books; books_pointer < books + n; books_pointer++) {
        for (ca_pointer = city_authors; ca_pointer < city_authors + amount_of_structures && strcmp(ca_pointer->city, books_pointer->publisher.city); ca_pointer++)
            ; //try to find same cities

        if (ca_pointer == city_authors + amount_of_structures) { //there are no same cities - add new
            strcpy(ca_pointer->city, books_pointer->publisher.city); //add new city in array
            AUTHORS
            amount_of_structures++;
        } else
            AUTHORS
    }

    fprintf(stdout, "\n\tCit%s with maximum amount of authors (%d):\n", (amount_of_structures == 1) ? "y" : "ies", max_amount_of_authors);
    for (ca_pointer = city_authors; ca_pointer < city_authors + amount_of_structures; ca_pointer++)
        if (ca_pointer->amount_of_authors == max_amount_of_authors)
            fprintf(stdout, "\"%s\"\n", ca_pointer->city);
}