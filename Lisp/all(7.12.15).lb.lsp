; lambda - function with no name:
; 		(lambda (...) 
;			main
;		)
; setq - create user variable (name + value)
;
;(07.12.2015)
;log:
;(1)!! (2)?? (1)%%

; some math operations
	; NOT
	 (defun !! (n)
	  (cond
		( (= n nil) T )
		( t nil )
	  )
	 )
	; check whether it is only one element in lst
	 (defun ! (n)
	  (cond
		( (atom n) nil )
		( (and (atom (car n)) (null (cdr n))) T)
		( t nil )
	  )
	 )
;!!; works badly
	; rtrn mm element of a lst
	 (defun mn (n)
	  (cond
		( (null n) nil )
		( (atom n) n )
		( (! n) (car n) )
		( (< (car n) (mn (cdr n))) (car n) )
		( t (mn (cdr n)) )
	  )
	 )
	 ; palindrom
	 (defun plndrm (lst)
	  (cond
		( (or (null lst) (atom lst) (! lst)) T )
		( (= (car lst) (cir lst)) (plndrm (cdr (clr lst))) )
		( t nil )
	  )
	 )
	; whether lst consist of only one val
	 (defun l! (lst)
	  (cond
		( (null lst) nil )
		( (! lst) T )
		( (= (car lst) (cadr lst)) (l! (cdr lst)) )
		( t nil )
	  )
	 )
	; some arithmetic operations
		 (defun fct (n) 
		  (cond
			( (= n 0) 1 )
			( t (* n (fct (- n 1))) )
		  )
		 )
		 (defun sum (n)
		  (cond
			( (null n) 0 )
			( (atom n) n )
			( t (+ (car n) (sum (cdr n))) )
		  )
		 )
		 (defun avrg (n)
		  (cond
			( (null n) 0 )
			( (atom n) n )
			( t (/ (sum n) (length n)) )
		  )
		 )
		; greater common divisor
		 (defun gcd (a b)
		  (cond 
			( (= b 0) a )
			( t (gcd b (% a b)) )
		  )
		 )
		;gcd from lst
		 (defun lgcd (n)
		  (cond
			( (null n) nil )
			( (null (cdr n)) (car n) )
			( t (gcd (car n) (lgcd (cdr n))) )
		  )
		 )	
		 
; IMPRTNT!
	; appl fnctn to lst
	 (defun each (lst f)
	  (cond
		( (null lst) nil )
		( t (cons (funcall f (car lst)) (each (cdr lst) f)) )
	  )
	 )
	; rtrn a tail of lst
	 (defun cir (n)
	  (cond
		( (null n) nil )
		( (atom n) n )
		( (null (cdr n)) (car n) )
		( t (cir (cdr n)) )
	  )
	 )
	; rtrn a lst except tail
	 (defun clr (n)
	  (cond
		( (or (atom n) (! n)) nil )
		( t (cons (car n) (clr (cdr n))) )
	  )
	 )
	; rtrn lngth of lst
	 (defun lngth (n)
	  (cond
		( (null n) 0 )
		( (atom n) nil )
		( t (+ 1 (lngth (cdr n))) )
	  )
	 )
;??; rtrn depth of lst
	 (defun dpth (lst)
	  (cond
		( (atom lst) 0 )
		( (null lst) 1 ) 
		( t (max (+ 1 (dpth (car lst))) (dpth (cdr lst))) )
	  )
	 )
	; flip a lst
	 (defun rvrt (n)
	  (cond
		( (null n) nil )
		( (atom n) n)
		( t (cons (cir n) (rvrt (clr n))) )
	  )
	 )
	; SORT!
		; add an elmnt into a lst
			; for <
			 (defun insrt< (lst el) 
			  (cond
				( (null lst) (list el) )
				( (< el (car lst)) (cons el lst) )
				( t (cons (car lst) (insrt< (cdr lst) el)) )
			  )
			 )
			; for >
			 (defun insrt> (lst el) 
			  (cond
				( (null lst) (list el) )
				( (> el (car lst)) (cons el lst) )
				( t (cons (car lst) (insrt> (cdr lst) el)) )
			  )
			 )
			; both
			 (defun insrt (lst el key) 
			  (cond
				( (null lst) (list el) )
				( (= key '<)
				 (cond
					( (< el (car lst)) (cons el lst) )
					( t (cons (car lst) (insrt< (cdr lst) el)) )
				 )
				)
				( (= key '>)
				 (cond
					( (> el (car lst)) (cons el lst) )
					( t (cons (car lst) (insrt> (cdr lst) el)) )
				 )
				)
			  )
			 )
			; both+
			 (defun insrt+ (lst el) 
			  (cond
				( (! lst) nil )
				( (null lst) (list el) )
				( (< (car lst) (cir lst))
				 (cond
					( (< el (car lst)) (cons el lst) )
					( t (cons (car lst) (insrt< (cdr lst) el)) )
				 )
				)
				( (> (car lst) (cir lst))
				 (cond
					( (> el (car lst)) (cons el lst) )
					( t (cons (car lst) (insrt> (cdr lst) el)) )
				 )
				)
			  )
			 )
		; not_defined type of srt
			; for <
			 (defun srt< (lst)
			  (cond
				( (null lst) nil )
				( t (insrt< (srt< (cdr lst)) (car lst)) )
			  )
			 )
			; for >
			 (defun srt> (lst)
			  (cond
				( (null lst) nil )
				( t (insrt> (srt> (cdr lst)) (car lst)) )
			  )
			 )
			; both
			 (defun srt (lst key)
			  (cond
				( (null lst) nil )
				( (= key '<) (insrt< (srt< (cdr lst)) (car lst)) )
				( (= key '>) (insrt> (srt> (cdr lst)) (car lst)) )
			  )
			 )
		; binary tree
;??	;HOW TO MAKE OTPT AND READ IT? 
			; add to tree
			 (defun addttr (el tr)
			  (cond
				( (null tr) (list el nil nil) )
				( (= (car tr) el) tr )
				( (> (car tr) el) (list (car tr) (addttr el (cadr tr)) (caddr tr)) )
				( t (list (car tr) (cadr tr) (addttr el (caddr tr))) )
			  )
			 )
;*			(setq tr nil)
			; tree inpt
			 (defun trinpt nil
			  (or
				( setq tr nil )
				( loop
					( prints "    >>    " )
					( setq a (read) )
					 (cond
						( (= A 0) (return tr) )
					 )
					( setq tr (addttr a tr) )
				)
			  )
			 )
			 
; other
	; mix two sorted lsts
		; for <
		 (defun +sl< (a b)
		  (cond
			( (null a) b )
			( (null b) a )
			( (< (car a) (car b)) (cons (car a) (+sl< (cdr a) b)) )
			( t (cons (car b) (+sl< a (cdr b))) )
		  )
		 )
		; for >
		 (defun +sl> (a b)
		  (cond
			( (null a) b )
			( (null b) a )
			( (> (car a) (car b)) (cons (car a) (+sl> (cdr a) b)) )
			( t (cons (car b) (+sl> a (cdr b))) )
		  )
		 )
		; both
		 (defun +sl (a b key)
		  (cond
			( (and (null a) (null b)) nil )
			( (null a) b )
			( (null b) a )
			( (= key '>)
			 (cond
				( (> (car a) (car b)) (cons (car a) (+sl> (cdr a) b)) )
				( t (cons (car b) (+sl> a (cdr b))) )
			 )
			)	
			( (= key '<)
			 (cond
				( (< (car a) (car b)) (cons (car a) (+sl< (cdr a) b)) )
				( t (cons (car b) (+sl< a (cdr b))) )
			 )
			)
		  )
		 )
		; both+
		 (defun +sl+ (a b)
		  (cond
			( (or (and (null a) (null b)) (! a) (! b)) nil )
			( (null a) b )
			( (null b) a )
			( (> (car a) (cir a))
			 (cond
				( (> (car a) (car b)) (cons (car a) (+sl> (cdr a) b)) )
				( t (cons (car b) (+sl> a (cdr b))) )
			 )
			)	
			( (< (car a) (cir a))
			 (cond
				( (< (car a) (car b)) (cons (car a) (+sl< (cdr a) b)) )
				( t (cons (car b) (+sl< a (cdr b))) )
			 )
			)
		  )
		 )
		; both++
		 (defun +sl++ (a b)
		  (cond
			( (or (and (null a) (and (null b)) (! a) (! b))) nil )
			( (null a) b )
			( (null b) a )
			( (or (> (car a) (cir a)) (> (car b) (cir b)))
			 (cond
				( (> (car a) (car b)) (cons (car a) (+sl> (cdr a) b)) )
				( t (cons (car b) (+sl> a (cdr b))) )
			 )
			)	
			( (or (< (car a) (cir a)) (< (car b) (cir b)))
			 (cond
				( (< (car a) (car b)) (cons (car a) (+sl< (cdr a) b)) )
				( t (cons (car b) (+sl< a (cdr b))) )
			 )
			)
		  )
		 )
;%%;in progress (1)
	; is this a variety 
	 (defun +vr (el vrtr)
	  (cond
		( (null tr) (list el nil nil) )
		( (= (car tr) el) nil )
		( (> (car tr) el) (list (car tr) (addttr el (cadr tr)) (caddr tr)) )
		( t (list (car tr) (cadr tr) (addttr el (caddr tr))) )
	  )
	 )
	 (defun ?vr (lst)
	  (setq var nil)
	  (cond
		( (or (null lst) (atom lst)) nil )
		( (!! (= (lngth lst) 0)) var )
		( t (and (setq var (+vr (car lst) var)) (?vr (cdr lst))) )
	  )
	 )
	 

	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	