;(06.12.2015)
;log:
;(0)!! (1)?? (0)%%

; IMPRTNT!
	; appl fnctn to lst
	 (defun each (lst f)
	  (cond
		( (null lst) nil )
		( t (cons (funcall f (car lst)) (each (cdr lst) f)) )
	  )
	 )
	; rtrn a tail of lst
	 (defun cir (n)
	  (cond
		( (null n) nil )
		( (atom n) n )
		( (null (cdr n)) (car n) )
		( t (cir (cdr n)) )
	  )
	 )
	; rtrn a lst except tail
	 (defun clr (n)
	  (cond
		( (or (atom n) (! n)) nil )
		( t (cons (car n) (clr (cdr n))) )
	  )
	 )
	; rtrn lngth of lst
	 (defun lngth (n)
	  (cond
		( (null n) 0 )
		( (atom n) nil )
		( t (+ 1 (lngth (cdr n))) )
	  )
	 )
;??; rtrn depth of lst
	 (defun dpth (lst)
	  (cond
		( (atom lst) 0 )
		( (null lst) 1 ) 
		( t (max (+ 1 (dpth (car lst))) (dpth (cdr lst))) )
	  )
	 )
	; flip a lst
	 (defun rvrt (n)
	  (cond
		( (null n) nil )
		( (atom n) n)
		( t (cons (cir n) (rvrt (clr n))) )
	  )
	 )

;%%;in progress (0)