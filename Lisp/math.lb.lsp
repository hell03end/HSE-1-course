;(13.12.2015)
;log:
;(1)!! (0)?? (0)%%

; some math operations
	; NOT
	 (defun !! (n)
	  (cond
		( (= n nil) T )
		( t nil )
	  )
	 )
	; check whether it is only one element in lst
	 (defun ! (n)
	  (cond
		( (atom n) nil )
		( (and (atom (car n)) (null (cdr n))) T)
		( t nil )
	  )
	 )
	 ; palindrom
	 (defun plndrm (lst)
	  (cond
		( (or (null lst) (atom lst)) T )
		( (= (car lst) (cir lst)) (plndrm (cdr (clr lst))) )
		( t nil )
	  )
	 )
	; whether lst consist of only one val
	 (defun l! (lst)
	  (cond
		( (null lst) nil )
		( (! lst) T )
		( (= (car lst) (cadr lst)) (l! (cdr lst)) )
		( t nil )
	  )
	 )
	; whether lst consider element
	 (defun e! (el lst)
	  (cond
		( (null lst) nil )
		( (= (car lst) el) T )
		( t (e! el (cdr lst)) )
	  )
	 )
	; whether lst is a variety
	 (defun v?r (lst)
	  (cond
		( (or (null lst) (! lst)) T )
		( (member (car lst) (cdr lst)) nil )
		( t (v?r (cdr lst)) )
	  )
	 )
		; another realisation
		(defun ?vr (lst)
		  (cond
			( (or (null lst) (! lst)) T )
			( (e! (car lst) (cdr lst)) nil )
			( t (?vr (cdr lst)) )
		  )
		 )
;!!; works badly (long)
	; rtrn mm element of a lst
	 (defun mn (lst)
	  (cond
		( (null lst) nil )
		( (atom lst) lst )
		( (! lst) (car lst) )
		( (< (car lst) (mn (cdr lst))) (car lst) )
		( t (mn (cdr lst)) )
	  )
	 )
		; another realisation
		; rtrn mn element of a lst with sorting it
		 (defun mns (lst)
			( car (srt< lst) )
		 )
	; some arithmetic operations
		 (defun fct (n) 
		  (cond
			( (= n 0) 1 )
			( t (* n (fct (- n 1))) )
		  )
		 )
		 (defun sum (n)
		  (cond
			( (null n) 0 )
			( (atom n) n )
			( t (+ (sum (car n)) (sum (cdr n))) )
		  )
		 )
		 (defun avrg (n)
		  (cond
			( (null n) 0 )
			( (atom n) n )
			( t (/ (sum n) (length n)) )
		  )
		 )
		; greater common divisor
		 (defun gcd (a b)
		  (cond 
			( (= b 0) a )
			( t (gcd b (% a b)) )
		  )
		 )
		;gcd from lst
		 (defun lgcd (n)
		  (cond
			( (null n) nil )
			( (null (cdr n)) (car n) )
			( t (gcd (car n) (lgcd (cdr n))) )
		  )
		 )

;%%;in progress (0)