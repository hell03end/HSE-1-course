program lab3_p1;
var
   Eps,sum,exct,x,a: real;
   i: integer;
begin
     writeln('Task 1.');
     write('enter x, 0<x<1: ');
     read(x);
     while (x<=0) or (x>=1) do
           read(x);
     write('enter Eps, 0.00000001<Eps<1: ');
     readln(Eps);
     while (Eps<=0.00000001) or (Eps>=1) do
           read(Eps);
     exct:= power((1+x),-1/3);
     writeln('exact value is: ', exct:1:5, '.');
     sum:=1;
     i:= 1;
     a:= -(3*i-2)/(3*i);
     while (abs(a) > Eps) do begin
          sum:= sum + a;
          inc(i);
          a:= -a*(3*i-2)/(3*i);
     end;
     writeln('partical sum is: ', sum:1:5, '.');
     writeln('the difference is: ', exct-sum:1:5, ' (', abs((exct-sum)/exct*100):3:5, '%).');
end.