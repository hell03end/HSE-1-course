//log (11.12.15): done
//??(1) !!(0) &&(0)

program lb7_pr2;
function inpt: integer;
 var
	tmp: String;
	chk: integer;
 begin
	write('>> enter k: ');
	readln(tmp); // read string
	val(tmp,result,chk); // try to transform it into an integer
	if (chk <> 0) or (result < 1) or (result > 2) then
		inpt();
 end;
function el (s: String): boolean; // true if name is not a space
 var
 	i: integer;
 begin
 	result:= true;
 	for i:= 1 to length(s) do
 		if (s[i] = #2) or (s[i] = #9) or (s[i] = #152) or (s[i] in [#28..#32]) then
 			result:= false;
 end;
function npt (const stmp,tmp: String): String; // name inpt
 begin
	write('>> enter file name: ');
	read(result); //usr inpt of file name
	while (tmp + '\' + result + '.txt' = stmp) or not (el(result)) do begin //excpt spc
		write('  >> enter name again: ');
		read(result);
	end;
 end; 
procedure otpt (var fl: text);
 var
	tmp: String;
 begin
	reset(fl);
	while not (eof(fl)) do begin
		readln(fl,tmp);
		writeln(tmp);
	end;
 end;
function pth: String; //for usr inpt of the path
 begin //?? one extra loop
	write('>> enter the path: ');
	readln(result);
	while (CanCreateFile(result + '\ #%.txt') <> true) do begin // chk prprts
		writeln('  << can not be created!');
		write('  >> please, enter the path again: ');
		readln(result);		
	end;
 end;
procedure kon (var fl: text); // choose to close/save file
 var
	tmp: integer;
 begin
	writeln('>> enter [1] to delete file or [2] to save: ');
	tmp:= inpt();
	if (tmp = 1) then
		erase(fl); // delete file
 end;
procedure main;
 var
	fl,edfl: text;
	stmp,tmp: String;
	i: integer;
	key: boolean;
 begin
	writeln('    Tsk2');
{1}	stmp:= pth() + '\' + npt(stmp,tmp) + '.txt'; //creates file name
	assign(fl,stmp); // opens scourse file
	if (fileexists(stmp)) then begin
		reset(fl);
		otpt(fl); // otpt file
		reset(fl); // reopening file
{2}		tmp:= pth();
		stmp:= tmp + '\' + npt(stmp,tmp) + '.txt'; //creates file name
		assign(edfl,stmp); // opens new/edited file
		rewrite(edfl); // creates fl
{main}	while not (eof(fl)) do begin // until the end
			readln(fl,stmp);
			key:= false;
			for i:= 1 to length(stmp) do
				if (stmp[i] in ['0'..'9']) then begin
					stmp[i]:= chr(ord(stmp[i]) + 17); // change numbers to letters
					key:= true;
				end;
			if (key = true) then
				writeln(edfl,stmp); // write edited line in a new file
			key:= false;
		end;
		reset(edfl); // reopening file
		if (eof(edfl)) then begin
			writeln('  << there are no numbers to change.');
			close(edfl);
			erase(edfl);
		end else begin
			otpt(edfl); // otpt file
			close(edfl); // close edited file
//			kon(edfl); // offers choise to save or delete file
		end;
		close(fl); // close scourse file
//		kon(fl);
	end else
		writeln('  << no file found!');
 end;
begin
	cls(); // clear screen
	main();
end.