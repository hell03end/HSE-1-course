program lb4_pr1;
const
     nmax = 10;
     Mmax = 20;
var
   a: array[1..nmax, 1..Mmax] of integer;
   n,m,zr,lnz:integer;
procedure otpt;
 var
    i,j: integer;
 begin
      for i:= 1 to n do begin
          for j:= 1 to m do
              write('{', a[i,j], '}');
          writeln();
      end;
 end;
procedure arcr;
 var
    i,j: integer;
    key: boolean;
 begin
      writeln('fill the array');
      for i:= 1 to n do begin
          key:= false;
          for j:= 1 to m do begin
              read(a[i,j]);
              if (a[i,j] = 0) and (zr = 0) then begin
                 zr:= i;
                 key:= true;
              end else if (a[i,j] = 0) then
                   key:= true;
          end;
          if (key = false) then
             lnz:= i;
      end;
 end;
procedure swtch;
 var
    i,tmp: integer;
 begin
      if (n < 2) or (zr = 0) or (lnz = 0) then
         writeln('array can not be changed.')
      else begin
          for i:= 1 to m do begin
              tmp:= a[zr,i];
              a[zr,i]:= a[lnz,i];
              a[lnz,i]:= tmp;
          end;
      otpt();
      end;
 end;
begin
     writeln('Tsk1');
     write('enter n, m: ');
     read(n,m);
     while (n <= 0) or (n > nmax) or (m <= 0) or (m > Mmax) do
           read(n,m);
     zr:= 0;
     lnz:= 0;
     arcr();{     otpt();     writeln();}
     swtch();
end.