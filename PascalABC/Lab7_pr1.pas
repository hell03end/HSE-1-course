//log (15.12.15): done
//??(1) !!(0) &&(0)

program lb7_pr1;	
function inpt: real;
 var
	tmp: String;
	chk: integer;
 begin
	write('>> ');
	readln(tmp); // read string
	val(tmp,result,chk); // try to transform it into an integer
	if (chk <> 0) then
		inpt();
 end;
procedure main;
 var
	fl: file of real; //typical file
	i,k: integer;
	tmp,min,km: real;
	stmp: String;
 begin
	stmp:= 'd:\\#tsk1.dat'; //creates file name
	assign(fl,stmp);
	rewrite(fl);
	write('>> enter k: ');
	k:= trunc(inpt());
	while (k <= 0) do begin
       write('>> enter k: ');
       k:= trunc(inpt());
  end;
	for i:= 1 to k do begin
//     read(km);
     write(fl,inpt());
  end;
	reset(fl); //read fl
	min:= maxreal;
	for i:= 1 to k do begin
		read(fl,tmp); // read strng
		if (i mod 2 = 0) and (tmp < min) then begin
			min:= tmp;
			writeln('        << ', tmp:0:3);
		end else
			writeln('  << ', tmp:0:3);
	end;
	close(fl);
//	erase(fl); // delete file
	if (min <> maxreal) then
		writeln('<< min is: ', min:0:3)
	else
		writeln('<< the error has occured: there are no min.');
 end;
begin
	cls(); //clear screen
	main();
end.