program lb5_pr3;
type pntr = ^link;
link = record
     dat,nmb: integer;
     nxt: pntr;
end;
var
   beg: pntr;
function rndm: integer;
 begin
    rndm:= trunc({power(-1,random(3))*}random(1,1000));
 end; 
procedure output (i: integer);
 var
    now_pntr: pntr;
 begin
      now_pntr:= beg;
      if (beg = nil) then
         writeln('<< sorry, there is no array.')
      else begin
           while (now_pntr <> nil) do begin
{1}              if (i = 1) then
                    write('{', now_pntr^.dat, '}')
{2}              else
                     writeln('<< {', now_pntr^.dat, '}', '-[', now_pntr^.nmb, ']');
                 now_pntr:= now_pntr^.nxt;
           end;
           if (i = 1) then
              writeln();
      end;
 end;
procedure arcr;
 var
    i,n: integer;
    tmp: real;
    now_pntr: pntr;
 begin
{inpt}write('>> enter n: ');
      read(tmp);
      while (trunc(tmp) <= 0) or (trunc(tmp) > 1000) do begin
          write('  >> enter the value again: ');
          read(tmp);
      end;
      n:= trunc(tmp); {usr
      n:= rndm(); 
      writeln('  >> [', n, ']');{chck}
      beg:= nil;
      write('>> fill in the array: ');
      for i:= 1 to n do begin
          if beg <> nil then begin
             new(now_pntr^.nxt);
             now_pntr:= now_pntr^.nxt;
          end else begin
              new(now_pntr);
              beg:= now_pntr;
          end;
{inpt}    read(tmp);
          while (frac(tmp) <> 0) do begin
              write('  >> enter the value again: ');
              read(tmp);
          end;
          now_pntr^.dat:= trunc(tmp); {usr
          now_pntr^.dat:= rndm(); {chck}
      end;
      now_pntr^.nxt:= nil;
      output(1);
 end;
procedure tsk3_pr1 (var beg: pntr);
 var
    now_pntr,prv_pntr,tmp_pntr: pntr;
 begin
      now_pntr:= beg;
      while (now_pntr <> nil) do begin
            prv_pntr:= now_pntr;
            tmp_pntr:= now_pntr^.nxt;
            now_pntr^.nmb:= 1;
            while (tmp_pntr <> nil) do
{dl_sm}           if (tmp_pntr^.dat = now_pntr^.dat) then begin
                     inc(now_pntr^.nmb);
                     prv_pntr^.nxt:= tmp_pntr^.nxt;
                     dispose(tmp_pntr);
                     tmp_pntr:= prv_pntr^.nxt;
{skp}             end else begin
                      prv_pntr:= tmp_pntr;
                      tmp_pntr:= tmp_pntr^.nxt;
                  end;
{nxt}       now_pntr:= now_pntr^.nxt;
      end;
      output(2);
 end;
procedure tsk3_pr2 (var beg: pntr);
 var
    now_pntr,prv_pntr: pntr;
 begin
      now_pntr:= beg;
      while (now_pntr <> nil) do
{skp}       if (now_pntr^.nmb = 1) then begin
               prv_pntr:= now_pntr;
               now_pntr:= now_pntr^.nxt;
{frst}      end else if (now_pntr^.nmb <> 1) and (now_pntr = beg) then begin
                beg:= beg^.nxt;
                dispose(now_pntr);
                now_pntr:= beg;
{dl}        end else if (now_pntr^.nmb <> 1) then begin
                prv_pntr^.nxt:= now_pntr^.nxt;
                dispose(now_pntr);
                now_pntr:= prv_pntr^.nxt;
            end;
      output(1);
 end;
begin
     writeln('  Tsk 3');
     arcr();
     tsk3_pr1(beg);
     tsk3_pr2(beg);
end.