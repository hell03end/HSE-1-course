program lb4_pr2;
const
     nmax = 10;
var
   a: array[1..nmax] of integer;
   n: integer;
procedure otpt;
 var
    i: integer;
 begin
      for i:= 1 to n do
          write('{', a[i], '}');
      writeln();
 end;
procedure sum;
 var
    i,j,tmp: integer;
 begin
      for i:= 1 to n do begin
          tmp:= a[i];
          a[i]:= 0;
          while (tmp div 10 <> 0) do begin
                a[i]:= a[i] + tmp mod 10;
                tmp:= tmp div 10;
          end;
          a[i]:= a[i] + tmp;
      end;
      otpt();
 end;
procedure arcr;
 var
    i: integer;
    key: boolean;
 begin
      writeln('fill the array with positive numbers');
      key:= true;
      i:= 1;
      while (i <= n) and (key = true) do begin
          read(a[i]);
          if (a[i] < 0) then
             key:= false;
          inc(i);
      end;
      if (key = true) then begin
         {otpt(); writeln();} sum();
      end else
          writeln('only positive numbers!');
 end;
begin
     writeln('Tsk2');
     write('enter n: ');
     read(n);
     while (n <= 0) or (n > nmax) do
           read(n);
     arcr();
end.