program lab3_p2;
type pntr = ^link;
link = record
      dat: integer;
      nxt: pntr;
end;
type strt = ^adrs;
adrs = record
     dat,nmb: integer;
     beg: pntr;
     nxt: strt;
end;
var
   beg: strt;
procedure output;
          var
             now_pntr: pntr;
             lnk_pntr: strt;
          begin
               lnk_pntr:= beg;
               while (lnk_pntr <> nil) do begin
                     write('{', lnk_pntr^.dat, '}');
                     now_pntr:= lnk_pntr^.beg;
                     while (now_pntr <> nil) do begin
                           write('{', now_pntr^.dat, '}');
                           now_pntr:= now_pntr^.nxt;
                     end;
                     writeln();
                     lnk_pntr:= lnk_pntr^.nxt;
               end;
          end;
procedure pntr_cr (lnk_pntr: strt; n: integer);
          var
             i: integer;
             now_pntr: pntr;
          begin
               write('fill in the row of the matrix: ');
               read(lnk_pntr^.dat);
               if (n <> 1) then begin
                  new(now_pntr);
                  lnk_pntr^.beg:= now_pntr;
                  for i:= 1 to n-2 do begin
                      read(now_pntr^.dat);
                      new(now_pntr^.nxt);
                      now_pntr:= now_pntr^.nxt;
                  end;
                  read(now_pntr^.dat);
                  now_pntr^.nxt:= nil;
               end
               else
                   lnk_pntr^.beg:= nil;
          end;
procedure arcr;
          var
             i,n: integer;
             lnk_pntr: strt;
          begin
               write('enter n: ');
               read(n);
               while (n<=0) or (n>1000) do
                     read(n);
               if (n < 5)then
                  writeln('all elements will be minimums!');
               beg:= nil;
               for i:= 1 to n do begin
                   if (beg <> nil) then begin
                      new(lnk_pntr^.nxt);
                      lnk_pntr:= lnk_pntr^.nxt;
                      pntr_cr(lnk_pntr, n)
                   end
                   else begin
                        new(lnk_pntr);
                        beg:= lnk_pntr;
                        pntr_cr(lnk_pntr, n);
                   end;
                   lnk_pntr^.nmb:= i;
               end;
               lnk_pntr^.nxt:= nil;
               if (lnk_pntr^.nmb = 1) then
                  writeln('there is no main diagonal, matrix is one-dimensional!');
               output();
          end;
procedure sort;
          var
             str_pntr,now_pntr,prv_pntr: pntr;
             tmp,i: integer;
             key: boolean;
          begin
               i:= 1;
               new(prv_pntr); //replace lnk_pntr for some time
               prv_pntr^.dat:=beg^.dat;
               prv_pntr^.nxt:= beg^.beg;
               str_pntr:= prv_pntr;
               now_pntr:= str_pntr^.nxt;
               key:= false;
               while (key = false) do begin
                     while (now_pntr <> nil) do begin
                           key:= true;
                           if (abs(now_pntr^.dat) < abs(prv_pntr^.dat)) then begin
                              tmp:= now_pntr^.dat;
                              now_pntr^.dat:= prv_pntr^.dat;
                              prv_pntr^.dat:= tmp;
                              key:= false;
                           end;
                           prv_pntr:= now_pntr;
                           now_pntr:= now_pntr^.nxt;
                           inc(i);
                     end;
                     prv_pntr:= str_pntr;
                     now_pntr:= str_pntr^.nxt;
               end;
               beg^.dat:= str_pntr^.dat; //getting things back
   {output}    now_pntr:= str_pntr;
               write('min: ');
               if (i > 4) then
                  for i:= 1 to 4 do begin
                      write('{', now_pntr^.dat, '}');
                      now_pntr:= now_pntr^.nxt;
                  end
               else
                   while (now_pntr <> nil) do begin
                         write('{', now_pntr^.dat, '}');
                         now_pntr:= now_pntr^.nxt;
                   end;
               writeln();
          end;
procedure sum;
          var
             sm,i: integer;
             now_pntr: pntr;
             lnk_pntr: strt;
          begin
               lnk_pntr:= beg^.nxt;
               sm:= 0;
               while (lnk_pntr <> nil) do begin
                     if (lnk_pntr^.dat mod 2 <> 0) then
                        sm:= sm + lnk_pntr^.dat;
                     now_pntr:= lnk_pntr^.beg;
                     i:= 2;
                     while (lnk_pntr^.nmb - i <> 0) do begin
                           if (now_pntr^.dat mod 2 <> 0) then
                              sm:= sm + now_pntr^.dat;
                           now_pntr:= now_pntr^.nxt;
                           inc(i);
                     end;
                     lnk_pntr:= lnk_pntr^.nxt;
               end;
               if (sm = 0) then
                  writeln('something went wrong...')
               else
                   writeln('sum is: ', sm);
          end;
begin
     writeln('Task 2.');
     arcr();
     sort();
     writeln('Task 3.');
     sum();
end.