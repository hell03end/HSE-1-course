program lb4_pr1;
type pntr = ^link;
link = record
      dat: integer;
      nxt: pntr;
end;
type strt = ^adrs;
adrs = record
     beg: pntr;
     nxt,prv: strt;
end;
var
   beg: strt;
procedure otpt;
          var
             now_pntr: pntr;
             lnk_pntr: strt;
          begin
               lnk_pntr:= beg;
               while (lnk_pntr <> nil) do begin
                     now_pntr:= lnk_pntr^.beg;
                     while (now_pntr <> nil) do begin
                           write('{', now_pntr^.dat, '}');
                           now_pntr:= now_pntr^.nxt;
                     end;
                     writeln();
                     lnk_pntr:= lnk_pntr^.nxt;
               end;
          end;
procedure lncr (lnk_pntr: strt; m: integer, var zr_pntr: strt; var lst_pntr: strt, var key: boolean);
          var
             i: integer;
             now_pntr: pntr;
             flg: boolean;
          begin
               write('fill in the row of the matrix: >> ');
               new(now_pntr);
               lnk_pntr^.beg:= now_pntr;
               flg:= true;
               for i:= 1 to m-1 do begin
                   read(now_pntr^.dat);
                   if (now_pntr = 0) and (key = false) then begin
                      key:= true;
                      flg:= false
                      zr_pntr:= lnk_pntr;
                   end
                   else if (now_pntr = 0) then
                        flg:= false;
                   if (flg = true) and (key = true) then
                      lst_pntr:= lnk_pntr;
                   new(now_pntr^.nxt);
                   now_pntr:= now_pntr^.nxt;
               end;
               read(now_pntr^.dat);
               if (now_pntr = 0) and (key = false) then begin
                  key:= true;
                  zr_pntr:= lnk_pntr;
               end
               else if (now_pntr = 0) then
                    flg:= false;
               if (flg = true) and (key = true) then
                  lst_pntr:= lnk_pntr;
               now_pntr^.nxt:= nil;
          end;
procedure arcr(n,m: integer; var zr_pntr: strt; var lst_pntr: strt);
          var
             i: integer;
             lnk_pntr: strt;
             key: boolean;
          begin
               beg:= nil;
               key:= false;
               for i:= 1 to n do begin
                   if (beg <> nil) then begin
                      new(lnk_pntr^.nxt);
                      lnk_pntr:= lnk_pntr^.nxt;
                      lncr(lnk_pntr, m, zr_pntr, lst_pntr, key)
                   end
                   else begin
                        new(lnk_pntr);
                        lnk_pntr^.prv:= nil;
                        beg:= lnk_pntr;
                        lncr(lnk_pntr, m, zr_pntr, lst_pntr, key);
                   end;
               end;
               lnk_pntr^.nxt:= nil;
               otpt();
          end;
procedure chng (zr_pntr,lst_pntr: strt);
          var
             str_pntr,now_pntr,prv_pntr: pntr;
             tmp,i: integer;
          begin
               i:= 1;
               new(prv_pntr); //replace lnk_pntr for some time
               prv_pntr^.dat:=beg^.dat;
               prv_pntr^.nxt:= beg^.beg;
               str_pntr:= prv_pntr;
               now_pntr:= str_pntr^.nxt;
               while (prv_pntr^.nxt <> nil) do begin
                     while (now_pntr <> nil) do begin
                           if (abs(now_pntr^.dat) < abs(prv_pntr^.dat)) then begin
                              tmp:= now_pntr^.dat;
                              now_pntr^.dat:= prv_pntr^.dat;
                              prv_pntr^.dat:= tmp;
                           end;
                           now_pntr:= now_pntr^.nxt;
                     end;
                     prv_pntr:= prv_pntr^.nxt;
                     now_pntr:= prv_pntr^.nxt;
                     inc(i);
               end;
               beg^.dat:= str_pntr^.dat; //getting things back
   {output}    now_pntr:= str_pntr;
               write('min: ');
               if (i > 4) then
                  for i:= 1 to 4 do begin
                      write('{', now_pntr^.dat, '}{');
                      now_pntr:= now_pntr^.nxt;
                  end
               else
                   while (now_pntr <> nil) do begin
                         write('{', now_pntr^.dat, '}{');
                         now_pntr:= now_pntr^.nxt;
                   end;
               writeln();
          end;
procedure main;
          var
             bttn,n,m: integer;
             zr_pntr,lst_pntr: strt;
          begin
               write('enter n, m: >> ');
               read(n,m);
               while ((n<=0) or (n>1000)) and ((m<=0) or (m>1000)) do
                     read(n,m);
               if (n < 2)then begin
                  writeln('array can not be changed, press');
                  writeln('      0 to continue');
                  writeln('      1 to restart');
                  writeln('      2 to stop');
                  write('>> ');
                  read(bttn);
                  while (bttn < 0) or (bttn > 2) do
                        read(bttn);
                  if (bttn = 0) then
                     arcr(n, m, zr_pntr, lst_pntr)
                  else if (bttn = 1) then
                       main();
               end
               else begin
                   arcr(n, m, zr_pntr, lst_pntr);
                   chng();
               end;
          end;
{procedure sum;
          var
             sm,i: integer;
             now_pntr: pntr;
             lnk_pntr: strt;
          begin
               lnk_pntr:= beg^.nxt;
               sm:= 0;
               while (lnk_pntr <> nil) do begin
                     if (lnk_pntr^.dat mod 2 <> 0) then
                        sm:= sm + lnk_pntr^.dat;
                     now_pntr:= lnk_pntr^.beg;
                     i:= 2;
                     while (lnk_pntr^.nmb - i <> 0) do begin
                           if (now_pntr^.dat mod 2 <> 0) then
                              sm:= sm + now_pntr^.dat;
                           now_pntr:= now_pntr^.nxt;
                           inc(i);
                     end;
                     lnk_pntr:= lnk_pntr^.nxt;
               end;
               if (sm = 0) then
                  writeln('something went wrong...')
               else
                   writeln('sum is: ', sm);
          end; }
begin
     writeln('Task 1');
     main();
end.