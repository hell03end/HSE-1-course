//log (11.12.15): done
//??(1) !!(0) &&(0)

program lb7_pr3;
function inpt: real;
 var
	tmp: String;
	chk: integer;
 begin
	readln(tmp); // read string
	val(tmp,result,chk); // try to transform it into an integer
	if (chk <> 0) or (result <= 0) then
		inpt();
 end;
function el (s: String): boolean; // true if name is not a space
 var
 	i: integer;
 begin
 	result:= true;
 	for i:= 1 to length(s) do
 		if (s[i] = #2) or (s[i] = #9) or (s[i] = #152) or (s[i] in [#28..#32]) then
 			result:= false;
 end;
procedure main;
type options = record
	y: integer; // year
	p,s: real; // p - price, s - size
 end;
type made = record
	c,f: String; // c - city, f - factory
 end;
type str = record
	md: made; // produser
	nm: string; // name
	pr: options;
 end;
 var
	fl,edfl: text;
	k,i,j: integer;
	stmp: String;
	tmp: real;
	tv: array[1..20] of str;
	ttv: str;
	key: boolean;
 begin
	assign(fl,'d:\\ #tsk3.txt');
	assign(edfl,'d:\\ #tsk3t.txt');
	write('>> enter k: ');
	k:= trunc(inpt());
	while (k > 20) do
		k:= trunc(inpt());
	rewrite(fl); // creates fl if it isn't excst
	rewrite(edfl);
	for i:= 1 to k do begin //creates file + record
{nm}	write('>> enter name: ');
		readln(tv[i].nm);
		while not (el(tv[i].nm)) do // something should be written here
			readln(tv[i].nm);
		writeln(fl, tv[i].nm); // >> write inptd data in file
{c}		write('>> enter city: ');
		readln(tv[i].md.c);
		while not (el(tv[i].md.c)) do // something should be written here
			readln(tv[i].md.c);
		writeln(fl, tv[i].md.c); // >>
{f}		write('>> enter factory: ');
		readln(tv[i].md.f);
		while not (el(tv[i].md.f)) do // something should be written here
			readln(tv[i].md.f);
		writeln(fl, tv[i].md.f); // >>
{y}		write('>> enter year: ');
		tv[i].pr.y:= trunc(inpt());
		while (tv[i].pr.y < 1911) or (tv[i].pr.y > 2015) do // frst tv was created in 1991
			tv[i].pr.y:= trunc(inpt());
		writeln(fl, tv[i].pr.y); // >>
{p}		write('>> enter pice: ');
		tv[i].pr.p:= trunc(inpt()); // any real number without specific conditions
		writeln(fl, tv[i].pr.p); // >>
{s}		write('>> enter screen size: ');
		tv[i].pr.s:= inpt(); // any real number without specific conditions
		writeln(fl, tv[i].pr.s); // >>
		writeln(fl,' ');
	end;
	for i:= 1 to k - 1 do
		for j:= i + 1 to k do begin
			if (tv[i].pr.s < tv[j].pr.s) then begin
				ttv:= tv[i];
				tv[i]:= tv[j];
				tv[j]:= ttv;
			end;
		end;
	reset(fl);
	for i:= 1 to k do
		writeln(edfl,tv[i].nm, ' ', tv[i].md.c, ' ', tv[i].md.f, ' ', tv[i].pr.y, ' ', tv[i].pr.p, ' ', tv[i].pr.s);
	close(fl);
	close(edfl);
	if (tv[1].pr.s = tv[k].pr.s) then
    writeln('<< �������������');
 end;
begin
	cls(); //clear screen
	main();
end.