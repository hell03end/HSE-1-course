# PascalABC works
Данный раздел содержит выполнение заданий по курсу «Алгоритмизация вычислений 1 курс (code 65042)» (НИУ ВШЭ, 2015). Содержимое раздела распространяется на условиях лицензии [MIT License](https://github.com/hell03end/HSE-1-course/blob/master/LICENSE). При использовании материалов обязательно упоминание автора работ. При наличии технической возможности необходимо также указать активную гиперссылку на [репозиторий автора](https://github.com/hell03end/) или, собственно, на данный раздел.

Works done with use of *PascalABC*.

**`final_project_KMP`** is made with use of [PascalABC.Net](http://pascalabc.net/en/).

Tasks can be found in "**`src/`**".

**Made by [hell03end](https://github.com/hell03end/) (except for `INTRST/`).**

2015.
