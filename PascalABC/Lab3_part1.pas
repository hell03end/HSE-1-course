program lab3_p1;
var
   Eps,sum,exct,x,a: real;
   i: integer;
begin
     writeln('Task 1.');
     write('enter x, 0<x<1: ');
     read(x);
     while (x<=0) or (x>=1) do
           read(x);
     write('enter Eps: ');
     readln(Eps);
     while (Eps<0) do
           read(Eps);
     exct:= power((1+x),-1/3);
     writeln('exact value is: ', exct, '.');
     sum:=1;
     i:= 1;
     a:= (power(-1,i)*(1+3*(i-1))*power(x,i))/(3*i);
     while (abs(a) > Eps) do begin
          sum:= sum + a;
          inc(i);
          a:= (power(-1,i)*(1+3*(i-1))*power(x,i))/(3*i);
     end;
     writeln('partical sum is: ', sum, '.');
     writeln('the difference is: ', exct-sum, '.');
end.