program lb5_pr2;
const
  nmax = 10;
  mmax = 21;
var
   n,m: integer;
   c: array[1..nmax, 1..mmax] of integer;
procedure inpt;
 var
    a,b: real;
 begin
      write('>> enter n, m: ');
      read(a,b);
      while (trunc(a) <= 0) or (trunc(b) <= 0) or (trunc(a) > nmax) or (trunc(b) > mmax - 1) or (trunc(b) < 2) do begin
          write('  >> enter values again: '); 
          read(a,b);
      end;
      n:= trunc(a); 
      m:= trunc(b);
 end;
procedure tsk2 (var n,m: integer);
 var
    i,j,rcknrll: integer;
    fstpd_btch: real;
 begin
      writeln('>> enter the array (of INTEGER):');
      for i:= 1 to n do begin
          for j:= 1 to m do begin
              read(fstpd_btch);
              while frac(fstpd_btch) <> 0 do begin
                    writeln('  >> enter the value, fstpd_btch, again:');
                    read(fstpd_btch);
              end;
              c[i,j]:= trunc(fstpd_btch);
          end;
      end;
      rcknrll:= maxint;
{srch}for i:= 1 to n do begin
          fstpd_btch:= 0;
          for j:= 1 to m-1 do
              if (abs(c[i,j] - c[i,j+1]) > abs(trunc(fstpd_btch))) then
                 fstpd_btch:= abs(c[i,j] - c[i,j+1]);
          c[i,m+1]:= trunc(fstpd_btch);
          if (c[i,m+1] < rcknrll) then
             rcknrll:= c[i,m+1];
      end;
{otpt}for i:= 1 to n do begin
          write('  << ');
          for j:= 1 to m do
              write('{', c[i,j], '}');
          write('[', c[i,j+1], ']  ');
          writeln();
      end;
{answ}writeln('<< v = ', rcknrll, '.');
 end;
begin
     writeln('  Tsk 2');
     inpt();
     tsk2(n,m);
end.