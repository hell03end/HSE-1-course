//log (11.12.15): done
//??(1) !!(0) &&(0)

program lb7_pr3;
function inpt: real;
 var
	tmp: String;
	chk: integer;
 begin
	readln(tmp); // read string
	val(tmp,result,chk); // try to transform it into an integer
	if (chk <> 0) or (result <= 0) then
		inpt();
 end;
function pth: String; //for usr inpt of the path
 begin //?? one extra loop
	write('>> enter the path: ');
	readln(result);
	while (CanCreateFile(result + '\ #%.txt') <> true) do begin // chk prprts
		writeln('  << can not be created!');
		write('  >> please, enter the path again: ');
		readln(result);
	end;
 end;
function el (s: String): boolean; // true if name is not a space
 var
 	i: integer;
 begin
 	result:= true;
 	for i:= 1 to length(s) do
 		if (s[i] = #2) or (s[i] = #9) or (s[i] = #152) or (s[i] in [#28..#32]) then
 			result:= false;
 end;
procedure kon (var fl: text); // choose to close/save file
 var
	tmp: real;
 begin
	writeln('>> enter [1] to delete file or [2] to save: ');
	tmp:= inpt();
	while (trunc(tmp) <> 1) and (trunc(tmp) <> 2) do
		tmp:= trunc(inpt());
	if (trunc(tmp) = 1) then
		erase(fl); // delete file
 end;
function avrg (var a: array[1..20] of real; var k: integer): real;
 var
 	i: integer;
 begin
 	result:= 0;
 	for i:= 1 to k do
 		result:= result + a[i];
 	result:= result / i;
 end;
function find (a: array[1..20] of String; var k: integer; var stmp: String): integer; // return first same string was found
 var
 	i: integer;
 begin
 	result:= -1;
 	i:= 1;
 	while (i <= k) and (result = -1) do begin
 		if (a[i] = stmp) then
 			result:= i;
 		inc(i);
 	end;
 end;
procedure main;
type options = record
	y: array[1..20] of integer; // year
	p,s: array[1..20] of real; // p - price, s - size
 end;
type made = record
	c,f: array[1..20] of String; // c - city, f - factory
 end;
type str = record
	md: made; // produser
	nm: array[1..20] of string; // name
	pr: options;
 end;
 var
	fl: text;
	k,i: integer;
	stmp: String;
	tmp: real;
	tv: str;
	key: boolean;
 begin
	writeln('    Tsk3');
	stmp:= pth() + '\ #tsk3.txt'; //creates file name
	assign(fl,stmp);
	write('>> enter k: ');
	k:= trunc(inpt());
	while (k > 20) do
		k:= trunc(inpt());
	rewrite(fl); // creates fl if it isn't excst
	for i:= 1 to k do begin //creates file + record
{nm}	write('>> enter name: ');
			readln(tv.nm[i]);
			while not (el(tv.nm[i])) do // something should be written here
				readln(tv.nm[i]);
			writeln(fl, tv.nm[i]); // >> write inptd data in file
{c}		write('>> enter city: ');
			readln(tv.md.c[i]);
			while not (el(tv.md.c[i])) do // something should be written here
				readln(tv.md.c[i]);
			writeln(fl, tv.md.c[i]); // >>
{f}		write('>> enter factory: ');
			readln(tv.md.f[i]);
			while not (el(tv.md.f[i])) do // something should be written here
				readln(tv.md.f[i]);
			writeln(fl, tv.md.f[i]); // >>
{y}		write('>> enter year: ');
			tv.pr.y[i]:= trunc(inpt());
			while (tv.pr.y[i] < 1911) or (tv.pr.y[i] > 2015) do // frst tv was created in 1991
				tv.pr.y[i]:= trunc(inpt());
			writeln(fl, tv.pr.y[i]); // >>
{p}		write('>> enter pice: ');
			tv.pr.p[i]:= trunc(inpt()); // any real number without specific conditions
			writeln(fl, tv.pr.p[i]); // >>
{s}		write('>> enter screen size: ');
			tv.pr.s[i]:= inpt(); // any real number without specific conditions
			writeln(fl, tv.pr.s[i]); // >>
			writeln(fl,' ');
	end;
	write('  >> enter the the city to search: ');
	readln(stmp); // this string should be found
	tmp:= avrg(tv.pr.s,k); // remember avrg
	key:= false;
	if (find(tv.md.c,k,stmp) = -1) then begin // there are no matchings
		writeln('<< found nothing.');
		writeln(fl,'  << end with error[1].');
		writeln(fl,'  << found nothing: there is no such cities[1].');
	end else begin
		for i:= 1 to k do
			if (tv.pr.s[i] > tmp) and (tv.md.c[i] = stmp) then begin
				writeln('<< ', tv.nm[i], ' ', tv.md.c[i], ' ', tv.md.f[i], ' ', tv.pr.y[i], ' ', tv.pr.p[i], ' ', tv.pr.s[i]);
				if (key = false) then
					writeln(fl, '  << result(s) was(were) found:');
				writeln(fl, '  << ', tv.nm[i], ' ', tv.md.c[i], ' ', tv.md.f[i], ' ', tv.pr.y[i], ' ', tv.pr.p[i], ' ', tv.pr.s[i]);
				key:= true;
			end;
		if (key = false) then begin // there are no matchings
			writeln('<< found nothing.');
			writeln(fl,'  << end with error[2].');
			writeln(fl,'  << found nothing: there is no such sizes[2].');
		end;
	end;
	close(fl);
	kon(fl); // offers choise to save or delete file
 end;
begin
	cls(); //clear screen
	main();
end.