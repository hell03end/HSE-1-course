program lb4_pr1;
const
     nmax = 10;
     Mmax = 20;
var
   a: array[1..nmax, 1..Mmax] of integer;
   n,m: real;
   zr,lnz:integer;
procedure otpt;
 var
    i,j: integer;
 begin
      for i:= 1 to trunc(n) do begin
          for j:= 1 to trunc(m) do
              write('{', a[i,j], '}');
          writeln();
      end;
 end;
procedure arcr;
 var
    fckng_btch: real;
    i,j: integer;
 begin
      writeln('fill the array (of INTEGER, gdmt!): ');
      for i:= 1 to trunc(n) do
          for j:= 1 to trunc(m) do begin
              read(fckng_btch);
              while (frac(fckng_btch) <> 0) do
                    read(fckng_btch);
              a[i,j]:= trunc(fckng_btch);
          end;
      i:= 1;
      j:= 1;
      while (i < trunc(n)) and (a[i,j] <> 0) do begin
            if (j <> 1) then
               inc(i);
            j:= 1;
            while (j < trunc(m)) and (a[i,j] <> 0) do
                  inc(j);
      end;
      if (a[i,j] = 0) then
         zr:= i;
      i:= trunc(n);
      while (i > 0) and (lnz = 0) do begin
            j:= 1;
            while (j < trunc(m)) and (a[i,j] <> 0) do
                  inc(j);
            if (a[i,j] <> 0) then
               lnz:= i;
            dec(i);
      end;
 end;
procedure swtch;
 var
    i,tmp: integer;
 begin
      if (trunc(n) < 2) or (zr = 0) or (lnz = 0) then
         writeln('array can not be changed.')
      else begin
          for i:= 1 to trunc(m) do begin
              tmp:= a[zr,i];
              a[zr,i]:= a[lnz,i];
              a[lnz,i]:= tmp;
          end;
      otpt();
      end;
 end;
begin
     writeln('Tsk1');
     write('enter n, m (INTEGER, gdmt!): ');
     readln(n,m);
     while (trunc(n) <= 0) or (trunc(n) > nmax) or (trunc(m) <= 0) or (trunc(m) > Mmax) or (frac(n) <> 0) or (frac(m) <> 0) do
           readln(n,m);
     n:= trunc(n);
     m:= trunc(m);
     zr:= 0;
     lnz:= 0;
     arcr();{     otpt();     writeln();}
     swtch();
end.