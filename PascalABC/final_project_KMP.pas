program KMP;
uses GraphABC; //for graphical inpt
type
	int = smallint; // it�s just more comfortable
	bool = boolean;
function otpt (var P: array of int; var H,N: String): String; // p-f array output
 begin
 	for var i: int:= Length(N) + 1 to Length(H) - 1 do
 		result += P[i] + ' ';
 end;
procedure clr (t: int); // pause + clear screen
 begin
 	sleep(t);
 	clearWindow();
 end;
function x (var H: String; i: int): int; // for dynamic x position on the screen
 begin
 	result:= 635 - TextWidth(H) div 2 + TextWidth(copy(H,1,i));
 end;
function pf (var P: array of int): int;
 begin
	DrawTextCentered(10,20,1270,240,'>> enter Needle string: '); // 2.2>>
	var N: String:= ReadString; // inpt N
	while (Length(N) = 0) or (Length(N) > 15) do // restrictions on inpt
		N:= ReadString;
	DrawTextCentered(10,80,1270,300,N);
	clr(700); // 2.3
	DrawTextCentered(10,20,1270,240,'>> enter Haystack string: ');
	var H: String:= ReadString; // inpt H
	while (Length(N) > Length(H)) or (Length(H) > 90) do // restrictions on inpt
		H:= ReadString;
	DrawTextCentered(10,80,1270,300,H);
	clr(700);
	DrawTextCentered(10,20,1270,240,'First, we create the S string, which consider both N and H strings devided by special symbol.');
	H:= N + #135 + H; // 1.1
	DrawTextCentered(10,80,1270,300, H);
	clr(2000);  // 2.4<<>>
	DrawTextCentered(10,20,1270,240,'Than, we compute a prefix-function for this S string.');
	sleep(2000);
	DrawTextCentered(10,50,1270,270,'We create an array of length of S, which will be used to store prefixes.');
	sleep(2000);
	DrawTextCentered(10,80,1270,300,'First element of this array is 0, because it has no prefix.');
	clr(2000);
	DrawTextCentered(10,50,1270,270,'next:');
	clr(700); // 2.5<<
	SetLength(p, Length(H)); // get memory for array P, 1.2
	P[0]:= 0; // 1.3
	var k: int;
	for var i: int:= 1 to Length(P) - 1 do
	begin
		k:= P[i - 1];
		SetFontSize(16); // 2.6>>
		SetFontColor(clBlack);
		DrawTextCentered(10,20,1270,240,H);
		TextOut(x(H,i + 1),140,'^'); // 2.7
		result:= k - 1;
		while (k > 0) and (H[i + 1] <> H[k + 1]) do
			k:= p[k - 1];
		if (H[i + 1] = H[k + 1]) then
		begin
			inc(k);
			TextOut(x(H,k - 1),140,'^');
		end else
			TextOut(x(H,result - 1),140,'x');
		p[i]:= k; // 1.4
		SetFontSize(10);
		SetFontColor(clGray);
		DrawTextCentered(10,80,1270,270,otpt(P,H,N)); // 2.8
		clr(300); // 2.9<<
	end;
	SetFontSize(18); // 2.10>>
	SetFontColor(clBlack);
	DrawTextCentered(10,10,1270,70,'result is:');
	DrawTextCentered(70,30,1210,170,otpt(P,H,N));
	sleep(2000); // <<
	result:= Length(N);
 end;
begin
	SetWindowSize(1280,360); // settings for otpt screen>>
	SetWindowIsFixedSize(True);
	SetWindowTitle('KMP: graphical output');
	CenterWindow();
	SetFontSize(18); // 2.1<<
	var P: array of int; // array for p-f
	var len: int:= pf(P); // prefix-function
	var key: bool:= false;  // 2.11>>
	var tmp: int;
	SetWindowHeight(640);
	CenterWindow();
	DrawTextCentered(10,400,1270,600,'Than, we search for prefixes, which matches with length of Needle string.');
	DrawTextCentered(10,430,1270,630,'Here, on i - Length of Needle string are our results.'); // <<
	sleep(2000);
	for var i: int:= len + 1 to Length(P) - 1 do
		if (P[i] = len) then
		begin
			writeln('<< ', i - 2 * len + 1);
			if (key = false) then
				tmp:= i - 2 * len + 1;  // 2.12>>
			key:= true;
		end;
	SetFontSize(30);
	if (key = true) then begin
		DrawTextCentered(10,200,1270,320,'<-- success!');
		DrawTextCentered(10,260,1270,400,tmp);
	end else
		DrawTextCentered(10,240,1270,360,'Nothing was found.');
	SetFontSize(14);
	SetFontColor(clGray);
	DrawTextCentered(10,580,1270,630,'press alt + F4 to out...');
end.