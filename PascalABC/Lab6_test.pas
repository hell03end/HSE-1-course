program lb6;
const
	kmax = 20;
type pntr = ^link;
link = record
     dat: String; //stored data
	   str_nmb: integer; //nmbr of basic string
     nxt: pntr; //next element
end;
var
	strwrs: array[1..kmax] of String;
	i,k,chk,tst: integer;
	tmp: String;
	beg,max: pntr; // frst and elmnts of list
function rndm: String; //returns random strng
 var
	ii,chk: integer; //lcl "i" and "chk" replace glbl (bad stl)
 begin
	delete(result,1,length(result)); //clear strng
	chk:= random(254); //lngth of new strng
	for ii:= 1 to chk do
		result:= result + chr(random(255)); //add rndm smbl to strng
 end;
procedure otpt;
 var
	now_pntr: pntr;
 begin
	now_pntr:= beg; //frst element
	tst:= 0;
	if (beg = nil) then //there are no elements in list
		writeln('<< sorry, there is no array.')
	else //in another case
		while (now_pntr <> nil) do begin //for 1 to end
      inc(tst);
			writeln(' << [', tst:3, '] ', now_pntr^.dat);
			now_pntr:= now_pntr^.nxt; // next element in list
		end;
 end;
//there is an err in this tsk !
procedure tsk1;
 var
	i,j: integer;
	now_pntr: pntr;
 begin
	writeln('   Tsk 1');
	tst:= 0;
	beg:= nil; // there is no lst
	for i:= 1 to k do begin
		chk:= 0; //prv rslt
		for j:= 1 to length(strwrs[i]) do // find rssn lttrs
			if (ord(strwrs[i][j]) in [192..255]) or (strwrs[i][j] = chr(168)) or (strwrs[i][j] = chr(184)) then begin
//				writeln(j:3, ' - ', strwrs[i][j]);
				if (j - chk <> 1) and (chk <> 0) then begin
{create lst}		if (beg <> nil) then begin //first element in list exist
						new(now_pntr^.nxt); //create next element
						now_pntr:= now_pntr^.nxt; //go to next element
					end else begin //first element in list isn't exist
						new(now_pntr); //create it
						beg:= now_pntr; //and remember it
					end;
					now_pntr^.dat:= copy(strwrs[i],chk+1,j-chk-1);
					now_pntr^.str_nmb:= i; //remember basic strng
					inc(tst);
				end;
				chk:= j; // nmbr of rssn lttr
			end;
	end;
	if (beg <> nil) then //if array was created
		now_pntr^.nxt:= nil; //lst element
	otpt();
	writeln('    [{', tst, '}]');
 end;
procedure tsk2;
 var
	now_pntr: pntr;
 begin
    writeln('   Tsk 2');
	now_pntr:= beg;
	max:= now_pntr; //frst element is max yet
	while (now_pntr <> nil) do begin
		if (length(now_pntr^.dat) > length(max^.dat)) then
			max:= now_pntr;
		now_pntr:= now_pntr^.nxt;
	end;
	writeln('<< max is: ', max^.dat, ' [', length(max^.dat), '] from {', max^.str_nmb, '}');
 end;
procedure tsk3;
 begin
    writeln('   Tsk 3');
	writeln('<< ', trimleft(strwrs[max^.str_nmb]));
 end;
begin
	write('>> enter k: ');
	readln(tmp); // read string
	val(tmp,k,chk); // try to transform it into an integer
	while (chk <> 0) or (k > kmax) or (k <= 0) do begin
		write('  >> enter the value again: ');
		readln(tmp);
		val(tmp,k,chk);
	end;
	write('>> enter [1] for usr inpt or [2] to rndm: ');
	readln(tmp);
	val(tmp,chk,i); // try to transform it into an integer
	while (i <> 0) or (chk > 2) or (chk <= 0) do begin
		write('  >> enter [1] or [2] again: ');
		readln(tmp);
		val(tmp,chk,i);
	end;
	for i:= 1 to k do begin // inpt of array
{usr}	if (chk = 1) then begin
			write('>> enter (', i, ') line: ');
			readln(strwrs[i]);
		end else
{chk}		strwrs[i]:= rndm(); {}
	end;
	for i:= 1 to k do // otpt
		writeln('  << ', strwrs[i]);
{1}	tsk1();
	if (beg <> nil) then begin //if array exist
{2}		tsk2();
{3}		tsk3(); {end}
	end else
		writeln('   End.');
end.