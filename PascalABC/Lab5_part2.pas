program lb5_pr2;
const
  nmax = 10;
  mmax = 21;
var
   n,m: integer;
   c: array[1..nmax, 1..mmax] of integer;
procedure rd (var a,b: String; var ta,tb: integer);
 begin
      readln(a);
      ta:= pos(' ',a); //only one value here
      if (ta = 0) then begin //in case if yes:
        readln(b); //then read another value
        ta:= pos(' ',a); //only one value here?
        if (ta <> 0) then //if not then:
          b:= copy(b,1,ta-1); //all other values will be ignored 
      end else begin //in case if not:
        b:= a;
        a:= copy(a,1,ta-1); //first value is a
        delete(b,1,ta); //all what after first is now b
        ta:= pos(' ',b); //is there smth else?
        if (ta <> 0) then begin
          b:= copy(b,1,ta-1); //b is the second value
        end;
      end;
      val(a,n,ta);
      val(b,m,tb);
 end;
procedure inpt;
 var
    a,b: String;
    ta,tb: integer;
 begin
         write('>> enter n, m: ');
         rd(a,b,ta,tb);
         while (ta <> 0) or (tb <> 0) or (n <= 0) or (m < 0) do begin
            write('  >> enter values again: ');
            rd(a,b,ta,tb);
         end;
 end;
procedure tsk2 (var n,m: integer);
 var
    i,j,rcknrll: integer;
    fstpd_btch: real;
 begin
      writeln('>> enter the array (of INTEGER):');
      for i:= 1 to n do begin
          for j:= 1 to m do begin
              read(fstpd_btch);
              while frac(fstpd_btch) <> 0 do begin
                    writeln('  >> enter the value, fstpd_btch, again:');
                    read(fstpd_btch);
              end;
              c[i,j]:= trunc(fstpd_btch);
          end;
      end;
      rcknrll:= maxint;
{srch}for i:= 1 to n do begin
          fstpd_btch:= 0;
          for j:= 1 to m-1 do
              if (abs(c[i,j] - c[i,j+1]) > abs(trunc(fstpd_btch))) then
                 fstpd_btch:= abs(c[i,j] - c[i,j+1]);
          c[i,m+1]:= trunc(fstpd_btch);
          if (c[i,m+1] < rcknrll) then
             rcknrll:= c[i,m+1];
      end;
{otpt}for i:= 1 to n do begin
          write('  << ');
          for j:= 1 to m do
              write('{', c[i,j], '}');
          write('[', c[i,j+1], ']  ');
          writeln();
      end;
{answ}writeln('<< v = ', rcknrll, '.');
 end;
begin
     writeln('  Tsk 2');
     inpt();
     tsk2(n,m);
end.