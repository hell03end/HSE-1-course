program atkin;
var
   is_prime: array[1..100001] of boolean;
   jj,a: longint;
procedure dosieve (limit: longint);
 var
    i,k,x,y,n: longint;
 begin
      for i:= 5 to limit do
          is_prime[i]:= false;
      for x:= 1 to trunc(sqrt(limit)) do
          for y:= 1 to trunc(sqrt(limit)) do begin
              n:= 4 * sqr(x) + sqr(y);
              if (n<=limit) and ((n mod 12 = 1) or (n mod 12 = 5)) then
                 is_prime[n]:= not(is_prime[n]);
              n:= n - sqr(x);
              if (n <= limit) and (n mod 12 = 7) then
                 is_prime[n]:= not(is_prime[n]);
              n:= n - 2 * sqr(y);
              if (x > y) and (n <= limit) and (n mod 12 = 11) then
                 is_prime[n]:= not(is_prime[n]);
          end;
      for i:= 5 to trunc(sqrt(limit)) do begin
          if (is_prime[i]) then begin
             k:= sqr(i);
             n:= k;
             while (n <= limit) do begin
                   is_prime[n]:= false;
                   n:= n + k;
             end;
          end;
      end;
      is_prime[2]:= true;
      is_prime[3]:= true;
 end;
begin
     write('enter n to find all simple numbers before it: ');
     readln(a);
     dosieve(a);
     a:= 1;
     write('{1}');
     for jj:= 1 to 10000 do
         if (is_prime[jj]) then begin
            write('{', jj, '}');
            inc(a);
         end;
     writeln('[', a, ']');
end.