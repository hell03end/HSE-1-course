program lab2;
type pntr = ^link;
link = record
     dat: real;
     nxt: pntr;
end;
var
   beg: pntr;
procedure output;
          var
             now_pntr: pntr;
          begin
               now_pntr:= beg;
               while now_pntr <> nil do begin
                     write('{', now_pntr^.dat:1:5, '}');
                     now_pntr:= now_pntr^.nxt;
               end;
               writeln();
          end;
procedure arcr;
         var
            i,n: integer;
            a,x,h: real;
            now_pntr: pntr;
         begin
              repeat
                    write('enter amount of the elements: ');
                    read(n);
              until (n>0);
              beg:= nil;
              write('enter a, x, h to calculate elements of array with r[i] = 0.8cos(8ax-ih): ');
              read(a,x,h);
              for i:= 1 to n do begin
                  if beg <> nil then begin
                     new(now_pntr^.nxt);
                     now_pntr:= now_pntr^.nxt;
                  end
                  else begin
                       new(now_pntr);
                       beg:= now_pntr;
                  end;
                  now_pntr^.dat:= 0.8*cos(8*a*x-i*h);
              end;
              now_pntr^.nxt:= nil;
              output();
         end;
procedure del;
         var
            prv_pntr,now_pntr: pntr;
         begin
              prv_pntr:= nil;
              now_pntr:= beg;
              while now_pntr <> nil do
                    if abs(now_pntr^.dat) < 0.7 then
                       if prv_pntr <> nil then begin
                          prv_pntr^.nxt:= now_pntr^.nxt;
                          dispose(now_pntr);
                          now_pntr:= prv_pntr^.nxt;
                       end
                       else begin
                            beg:= now_pntr^.nxt;
                            dispose(now_pntr);
                            now_pntr:= beg;
                       end
                    else begin
                         prv_pntr:= now_pntr;
                         now_pntr:= now_pntr^.nxt;
                    end;
              if beg = nil then
                 writeln('complete removal.')
              else
                  output();
         end;
procedure avrg;
          var
             now_pntr,neg_pntr,min_pntr: pntr;
             key: boolean;
             answ: real;
             n: integer;
          begin
               now_pntr:= beg;
               new(min_pntr);
               min_pntr^.dat:= 1E308;
               new(neg_pntr);
               neg_pntr^.dat:= 1;
               while now_pntr <> nil do begin
                   if now_pntr^.dat < min_pntr^.dat then begin
                      min_pntr:=now_pntr;
                      key:= false;
                   end;
                   if (now_pntr^.dat < 0) and (neg_pntr^.dat > 0) then begin
                      neg_pntr:= now_pntr;
                      key:= true;
                   end;
                   now_pntr:= now_pntr^.nxt
               end;
               if key = false then begin
                  now_pntr:= min_pntr;
                  min_pntr:= neg_pntr;
                  neg_pntr:= now_pntr;
               end;
               n:= 0;
               answ:= 0;
               now_pntr:= min_pntr^.nxt;
               if min_pntr = neg_pntr then
                  writeln('can not be calculated.')
               else if neg_pntr^.dat > 0 then
                    writeln('can not calculate average value, because there are only positive numbers in array.')
               else begin
                   while now_pntr <> neg_pntr do begin
                         answ:= answ + now_pntr^.dat;
                         now_pntr:= now_pntr^.nxt;
                         inc(n);
                   end;
                   if n = 1 then
                      writeln('there is only one value: ', answ:1:5, '.')
                   else
                       writeln('average value is: ', answ/n:1:5, '.');
               end;
          end;
begin
     writeln('Task 1.');
     arcr();

     writeln('Task 2.');
     del();
     
     if beg = nil then {writeln('...array was fully deleted.')}
     else begin
          writeln('Task 3.');
          avrg();
     end;
end.