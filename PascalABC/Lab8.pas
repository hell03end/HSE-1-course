//1. Создать связанный список (стек) для хранения целых
//чисел. Число записей неизвестно, но можно ввести число,
//являющееся признаком окончания ввода данных. Поменять
//местами первый мин и последний макс элементы.
//2. Для полученного списка решить следующую задачу:
//выбрать из списка элементы > A и < B и сформировать из
//этих элементов новый стек (числа A и B заданы).

program lb8;
type pntr = ^link;
link = record
	dat: integer;
	prv: pntr;
end;
var {glbl}
	top,pck: pntr;	
function inpt: integer;
 var
	tmp: String;
	chk: integer;
 begin
	write('		>> enter k: ');
	readln(tmp); // read string
	val(tmp,result,chk); // try to transform it into an integer
	while (chk <> 0) do begin
		write('			>>ERROR enter value again: ');
		readln(tmp);
		val(tmp,result,chk);
	end;
 end;
procedure otpt (beg: pntr); //not for stack now!
 var
	now_pntr: pntr;
 begin
	if (beg <> nil) then begin //there are no elements in list
		now_pntr:= beg; //frst element
		while (now_pntr <> nil) do begin //for 1 to end
			write('{', now_pntr^.dat, '} ');
			now_pntr:= now_pntr^.prv; // next element in list
		end;
	end else //in another case
		writeln('<< sorry, there is no array.')
 end;
function stcr (var el: integer; beg: pntr): pntr; //add new el in stack
 var
 	now_pntr: pntr;
 begin
 	new(now_pntr); //new elmnt
 	now_pntr^.dat:= el; //add val
 	if (beg <> nil) then begin //from lst to frst
 		now_pntr^.prv:= beg; //remember prvs elmnt
 		result:= now_pntr; //remember current elmnt
 	end else begin
 		now_pntr^.prv:= nil; //it's a frst elmnt, there are no elmnts before
 		result:= now_pntr; //remember current elmnt
 	end;
 end;
procedure brdr; //rules for addition
 var
 	k,el: integer;
 begin
 	writeln('>> enter border: ');
 	k:= inpt(); //border
 	writeln('>> enter elements: ');
 	el:= inpt(); //new element
 	while (el <> k) do begin
 		top:= stcr(el,top);
 		el:= inpt();
 	end;
 	otpt(top);
 end;
function fmin: pntr;
 var
 	now_pntr: pntr;
 begin
 	now_pntr:= top;
 	result:= top;
 	while (now_pntr <> nil) do begin
 		if (now_pntr^.dat < result^.dat) then
 			result:= now_pntr;
 		now_pntr:= now_pntr^.prv;
 	end;
 end;
function lmax: pntr;
 var
 	now_pntr: pntr;
 begin
 	now_pntr:= top;
 	result:= top;
 	while (now_pntr <> nil) do begin
 		if (now_pntr^.dat >= result^.dat) then
 			result:= now_pntr;
 		now_pntr:= now_pntr^.prv;
 	end;
 end;
procedure tsk1;
 var
	min,max: pntr;
	tmp: integer;
 begin
	brdr(); //creates a stack
	writeln();
	if (top = nil) then
		writeln(' <<ERROR: something hapened...')
	else begin
		min:= fmin(); //find fmin
		max:= lmax(); //find lmax
		if (min^.dat = max^.dat) then
			writeln(' <<ERROR: min == max. No changings.')
		else begin
			tmp:= min^.dat; //switch
			min^.dat:= max^.dat;
			max^.dat:= tmp;
			otpt(top);
			writeln();
		end;
	end;
 end;
procedure tsk2;
 var
	a,b: integer;
	now_pntr: pntr;
 begin
 	writeln('>> enter A, B (A > B): ');
	a:= inpt();
	b:= inpt();
	while (a <= b) do //a should be > than b
		b:= inpt(); 
	now_pntr:= top;
	while (now_pntr <> nil) do begin
		if (now_pntr^.dat < a) and (now_pntr^.dat > b) then //if > A or < B - add to new stack
			pck:= stcr(now_pntr^.dat,pck); //add element in new stack
		now_pntr:= now_pntr^.prv;
	end;
	otpt(pck);
 end;
begin
	cls(); //clear screen
	top:= nil; //there are no elements
	pck:= nil;
{1}	tsk1();
{2}	if (top <> nil) then
		tsk2(); {end}
end.