var
   i: integer;
begin
     cls();
     for i:= 1 to 255 do
         writeln('[', i:3, '] - ', chr(i));
end.