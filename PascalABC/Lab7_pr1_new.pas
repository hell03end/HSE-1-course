//log (15.12.15): done
//??(1) !!(0) &&(0)

program lb7_pr1;	
function inpt: real;
 var
	tmp: String;
	chk: integer;
 begin
	write('>> ');
	readln(tmp); // read string
	val(tmp,result,chk); // try to transform it into an integer
	if (chk <> 0) then
		inpt();
 end;
procedure main;
 var
	fl: file of real; //typical file
	i,k: integer;
	tmp,min,max: real;
	stmp: String;
 begin
	stmp:= 'd:\\ #tsk1.dat'; //creates file name
	assign(fl,stmp);
	write('>> enter k: ');
	k:= trunc(inpt());
	while (k <= 0) do begin
		write('>> enter k again: ');
		k:= trunc(inpt());
	end;
	writeln('>> enter [1] for usr inpt or [2] to rndm: ');
	tmp:= trunc(inpt());
	while (trunc(tmp) <> 1) and (trunc(tmp) <> 2) do
		tmp:= trunc(inpt());
	rewrite(fl);
	for i:= 1 to k do
		if (trunc(tmp) = 1) then
{usr}		write(fl,inpt())
		else begin
{chk}		min:= random(100000);
			while (min = 0) do // excpt 0
				min:= random(100000);
			write(fl,power(-1,random(3))*random(1000000000)/min);
		end;
	reset(fl); //read fl
	min:= maxreal;
	max:= -maxreal;
	for i:= 1 to k do begin
		read(fl,tmp); // read strng
		if (tmp < min) then 
			min:= tmp;
		if (tmp > max) then
			max:= tmp;
		writeln('  << ', tmp:0:3);
	end;
	close(fl);
//	erase(fl); // delete file
	if (min <> max) then begin
		writeln('<< min is: ', min:0:3);
		writeln('<< max is: ', max:0:3);
	end else
		writeln('<< warning: min == max.');
 end;
begin
	cls(); //clear screen
	main();
end.