program lb5_pr1;
var
   n,k: integer;
procedure inpt;
 var
    a,b: real;
 begin
      write('>> enter n, k, (k < n): ');
      read(a,b);
      while (trunc(a) < 0) or (trunc(b) < 0) or (trunc(b) > trunc(a)) or (trunc(a) > 250) do
         read(a,b);
      n:= trunc(a);
      k:= trunc(b);
 end;
function tsk1 (k,n: integer): real;
 var
    i: integer;
 begin
      tsk1:= 1;
      for i:= k to n do
          tsk1:= result * i;
 end;
begin
     writeln('  Tsk 1');
     inpt();
     if (n = 0) or (k = n) then
        writeln('<< a = 1.')
     else if (n - k <= k) then
          writeln('<< a = ', tsk1(k+1,n)/tsk1(1,n-k):1:0, '.')
     else
         writeln('<< a = ', tsk1(n-k+1,n)/tsk1(1,k):1:0, '.');
end.