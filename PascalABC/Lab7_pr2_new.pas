//log (11.12.15): done
//??(1) !!(0) &&(0)

program lb7_pr2;
function inpt: integer;
 var
	tmp: String;
	chk: integer;
 begin
	write('>> enter k: ');
	readln(tmp); // read string
	val(tmp,result,chk); // try to transform it into an integer
	if (chk <> 0) or (result < 1) or (result > 2) then
		inpt();
 end;
procedure otpt (var fl: text);
 var
	tmp: String;
 begin
	reset(fl);
	while not (eof(fl)) do begin
		readln(fl,tmp);
		writeln(tmp);
	end;
 end;
procedure main;
 var
	fl,edfl: text;
	stmp,s: String;
	i: integer;
	key: boolean;
 begin
 	write('>> enter name: ');
 	readln(s);
{1}	stmp:= 'd:\\' + i + '.txt'; //creates file name
	assign(fl,stmp); // opens scourse file
	if (fileexists(stmp)) then begin
		reset(fl);
		otpt(fl); // otpt file
		reset(fl); // reopening file
{2}		stmp:= 'd:\\ #12.txt'; //creates file name
		assign(edfl,stmp); // opens new/edited file
		rewrite(edfl); // creates fl
{main}	while not (eof(fl)) do begin // until the end
			readln(fl,stmp);
			key:= true;
			s:= stmp;
			for i:= 1 to length(s) do
				if (s[i] = ' ') then begin
					delete(s,i,1);
					dec(i);
				end;
			i:= 1;
			while (i <= length(s) div 2) and (key = true) do begin
				if (s[i] <> s[length(s) - i + 1]) then 
					key:= false;
				inc(i);
			end;
			if (key = true) then
				writeln(edfl,stmp); // write scourse line in a new file
			key:= true;
		end;
		reset(edfl); // reopening file
		if (eof(edfl)) then
			writeln('  << there are no numbers to change.');
		close(edfl); // close edited file
		close(fl); // close scourse file
	end else
		writeln('  << no file found!');
 end;
begin
	cls(); // clear screen
	main();
end.