program lb6;
const
	kmax = 20;
	pmax = 200;
type xep = record
	dat: String;
	bS: integer; //for basic strng number
end;
var
	strwrs: array[1..kmax] of String;
	pc: array[1..pmax] of xep;
	i,k,l,chk: integer;
	tmp: String;
procedure tsk1;
 var
	i,j: integer;
 begin
	l:= 0;
	for i:= 1 to k do begin
		chk:= 0; //prv rslt
		for j:= 1 to length(strwrs[i]) do // find rssn lttrs
			if (strwrs[i][j] = chr(139)) or (strwrs[i][j] = chr(155)) or (strwrs[i][j] = chr(60)) or (strwrs[i][j] = chr(62)) then begin
//				writeln(j:3, ' - ', strwrs[i][j]);
				if (j - chk <> 1) and (chk <> 0) and (strwrs[i][chk] = '<') and (strwrs[i][j] = '>') then begin
{create arr2}		inc(l);
					pc[l].dat:= copy(strwrs[i],chk+1,j-chk-1);
					pc[l].bS:= i; //remember basic strng
				end;
				chk:= j; // nmbr of rssn lttr
			end;
	end;
 end;
function tsk2: integer;
 var
	i,j,t,min: integer;
 begin
	min:= 255;
	for i:= 1 to l do begin
		t:= 0;
		j:= length(pc[i].dat);
		while (j <> 0) and (pc[i].dat[j] in ['0','1','2','3','4','5','6','7','8','9']) do begin
			inc(t);
			dec(j);
		end;
		if (t < min) then begin
			min:= t;
			result:= i;
		end;
	end;	
 end;
begin
	cls();
	write('enter k: ');
	readln(tmp); // read string
	val(tmp,k,chk); // try to transform it into an integer
	while (chk <> 0) or (k > kmax) or (k <= 0) do begin
		write('enter the value again: ');
		readln(tmp);
		val(tmp,k,chk);
	end;
	for i:= 1 to k do begin // inpt of array
		write('enter (', i, ') line: ');
		readln(strwrs[i]);
	end;
	for i:= 1 to k do // otpt
		writeln(strwrs[i]);
{1}	tsk1();
	for i:= 1 to l do
		writeln('  ', pc[i].dat);
{2}	writeln('min is: ', pc[tsk2()].dat);
	
end.