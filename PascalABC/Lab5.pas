program lb5;
type pntr = ^link;
link = record
     dat,nmb: integer;
     nxt: pntr;
end;
var {glbl}
   n,k: integer;
   beg: pntr;
procedure rd (nmb: integer; var a,b: String; var ta: integer);
 begin
    if (nmb = 1) then begin
      readln(a); // read string
      ta:= pos(' ',a); //only one value here
      if (ta <> 0) then //in case if not:
        a:= copy(a,1,ta-1); //a is the first value
      val(a,n,ta);
    end else if (nmb = 2) then begin
      readln(a);
      ta:= pos(' ',a); //only one value here
      if (ta = 0) then begin //in case if yes:
        readln(b); //then read another value
        ta:= pos(' ',b); //only one value here?
        if (ta <> 0) then //if not then:
          b:= copy(b,1,ta-1); //all other values will be ignored
      end else begin //in case if not:
        b:= a;
        a:= copy(a,1,ta-1); //first value is a
        delete(b,1,ta); //all what after first is now b
        ta:= pos(' ',b); //is there smth else?
        if (ta <> 0) then begin
          b:= copy(b,1,ta-1); //b is the second value
        end;
      end;
      val(a,n,ta); // transform it into an integer
      val(b,k,ta);
    end;
 end;
procedure inpt (nmbr: integer);
 var
    a,b: String;
    ta: integer;
 begin
      writeln('  Tsk', k);
{1,2} if (nmbr = 1) or (nmbr = 2) then begin
         write('>> enter n, k/m: ');
         rd(2,a,b,ta);
         while (ta <> 0) or (n <= 0) or (k < 0) do begin
            write('  >> enter values again: ');
            rd(2,a,b,ta);
         end;
{3}   end else begin
          write('>> enter n: ');
          rd(1,a,b,ta);
          while (ta <> 0) or (n <= 0) or (n > 1000) do begin
            write('  >> enter value again: ');
            rd(1,a,b,ta);
          end;
      end;
 end;
procedure output (i: integer);
 var
    now_pntr: pntr;
 begin
      now_pntr:= beg;
      if (beg = nil) then //there are no elements in list
         writeln('<< sorry, there is no array.')
      else begin //in another case
           while (now_pntr <> nil) do begin //for 1 to end
{1}              if (i = 1) then
                    write('{', now_pntr^.dat, '}')
{2}              else
                     writeln('<< {', now_pntr^.dat, '}', '-[', now_pntr^.nmb, ']');
                 now_pntr:= now_pntr^.nxt; //next element in list
           end;
{1+}       if (i = 1) then
              writeln();
      end;
 end;
procedure arcr;
 var
    i: integer;
    tmp: real;
    now_pntr: pntr;
 begin
      beg:= nil;
      write('>> fill in the array: ');
      for i:= 1 to n do begin
          if beg <> nil then begin //first element in list exist
             new(now_pntr^.nxt); //create next element
             now_pntr:= now_pntr^.nxt; //go to next element
          end else begin //first element in list isn't exist
              new(now_pntr); //create it
              beg:= now_pntr; //and remember it
          end;
{inpt}    read(tmp);
          while (frac(tmp) <> 0) do begin
              write('  >> enter the value again: ');
              read(tmp);
          end;
          now_pntr^.dat:= trunc(tmp); {usr
          now_pntr^.dat:= trunc(random(100)); {chck}
      end;
      now_pntr^.nxt:= nil;
      output(1);
 end;
function tsk1 (k,n: integer): real;
 var
    i: integer;
 begin
      tsk1:= 1;
      for i:= k to n do
          tsk1:= result * i;
 end;
procedure tsk2;
 var
    c: array[1..10,1..21] of integer;
    i,j,rcknrll: integer;
    fstpd_btch: real;
 begin
      writeln('>> enter the array (of INTEGER):');
      for i:= 1 to n do begin
          for j:= 1 to k do begin
              read(fstpd_btch);
              while frac(fstpd_btch) <> 0 do begin
                    writeln('>> enter the value, fstpd_btch, again:');
                    read(fstpd_btch);
              end;
              c[i,j]:= trunc(fstpd_btch);
          end;
      end;
      rcknrll:= maxint;
{srch}for i:= 1 to n do begin
          fstpd_btch:= 0;
          for j:= 1 to k-1 do
              if (abs(c[i,j] - c[i,j+1]) > abs(trunc(fstpd_btch))) then
                 fstpd_btch:= abs(c[i,j] - c[i,j+1]);
          c[i,k+1]:= trunc(fstpd_btch);
          if (c[i,k+1] < rcknrll) then
             rcknrll:= c[i,k+1];
      end;
{inpt}for i:= 1 to n do begin
          for j:= 1 to k do
              write('{', c[i,j], '}');
          write('  [', c[i,j+1], ']');
          writeln();
      end;
{answ}writeln('<< v = ', rcknrll, '.');
 end;
procedure tsk3_pr1;
 var
    now_pntr,prv_pntr,tmp_pntr: pntr;
 begin
      now_pntr:= beg;
      while (now_pntr <> nil) do begin
            prv_pntr:= now_pntr;
            tmp_pntr:= now_pntr^.nxt;
            now_pntr^.nmb:= 1;
            while (tmp_pntr <> nil) do
{del same}        if (tmp_pntr^.dat = now_pntr^.dat) then begin
                     inc(now_pntr^.nmb);
                     prv_pntr^.nxt:= tmp_pntr^.nxt;
                     dispose(tmp_pntr);
                     tmp_pntr:= prv_pntr^.nxt;
{skp}             end else begin
                      prv_pntr:= tmp_pntr;
                      tmp_pntr:= tmp_pntr^.nxt;
                  end;
{nxt}       now_pntr:= now_pntr^.nxt;
      end;
      output(2);
 end;
procedure tsk3_pr2;
 var
    now_pntr,prv_pntr: pntr;
 begin
      now_pntr:= beg;
      while (now_pntr <> nil) do
{skp}       if (now_pntr^.nmb = 1) then begin
               prv_pntr:= now_pntr;
               now_pntr:= now_pntr^.nxt;
{frst}      end else if (now_pntr^.nmb <> 1) and (now_pntr = beg) then begin
                beg:= beg^.nxt;
                dispose(now_pntr);
                now_pntr:= beg;
{del}       end else if (now_pntr^.nmb <> 1) then begin
                prv_pntr^.nxt:= now_pntr^.nxt;
                dispose(now_pntr);
                now_pntr:= prv_pntr^.nxt;
            end;
      output(1);
 end;
begin
{1}  k:= 1;
     inpt(k);
     while (k > n) or (n > 250) do begin
           k:= 1;
           inpt(k);
     end;
     if (n = 0) or (k = n) then
        writeln('<< a = 1.')
     else if (n - k <= k) then
          writeln('<< a = ', tsk1(k+1,n)/tsk1(1,n-k):1:0, '.')
     else
         writeln('<< a = ', tsk1(n-k+1,n)/tsk1(1,k):1:0, '.');
{2}  k:= 2;
     inpt(k);
     while (n > 10) or (k > 20) or (k < 2) do begin
           k:= 2;
           inpt(k);
     end;
     tsk2();
{3}  k:= 3;
     inpt(k);
     arcr();
     tsk3_pr1();
     tsk3_pr2();  {end}
end.