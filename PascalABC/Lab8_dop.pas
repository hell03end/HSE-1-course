program lb8;
type pntr = ^link;
link = record
	dat: integer;
	prv: pntr;
end;
var {glbl}
	top,pck: pntr;	
function inpt: integer;
 var
	tmp: String;
	chk: integer;
 begin
	write('		>> enter k: ');
	readln(tmp); // read string
	val(tmp,result,chk); // try to transform it into an integer
	while (chk <> 0) do begin
		write('			>>ERROR enter value again: ');
		readln(tmp);
		val(tmp,result,chk);
	end;
 end;
procedure otpt (beg: pntr); //not for stack now!
 var
	now_pntr: pntr;
 begin
	if (beg <> nil) then begin //there are no elements in list
		now_pntr:= beg; //frst element
		while (now_pntr <> nil) do begin //for 1 to end
			write('{', now_pntr^.dat, '} ');
			now_pntr:= now_pntr^.prv; // next element in list
		end;
	end else //in another case
		writeln('<< sorry, there is no array.')
 end;
function stcr (var el: integer; beg: pntr; h: integer): pntr; //add new el in stack
 var
 	now_pntr: pntr;
 begin
 	new(now_pntr); //new elmnt
 	now_pntr^.dat:= el; //add val
 	now_pntr^.prv:= nil;
 	if (h = 1) then
	 	if (top <> nil) then //from lst to frst
	 		beg^.prv:= now_pntr
	 	else
	 		top:= now_pntr
	else
		if (pck <> nil) then //from lst to frst
	 		beg^.prv:= now_pntr
	 	else
	 		pck:= now_pntr;
 	result:= now_pntr; //remember current elmnt
 end;
procedure brdr; //rules for addition
 var
 	k,el: integer;
 	tmp: pntr;
 begin
 	writeln('>> enter border: ');
 	k:= inpt(); //border
 	writeln('>> enter elements: ');
 	el:= inpt(); //new element
 	tmp:= nil;
 	while (el <> k) do begin
 		tmp:= stcr(el,tmp,1);
 		el:= inpt();
 	end;
 	otpt(top);
 end;
function lmax: pntr;
 var
 	now_pntr: pntr;
 begin
 	now_pntr:= top;
 	result:= top;
 	while (now_pntr <> nil) do begin
 		if (now_pntr^.dat >= result^.dat) then
 			result:= now_pntr;
 		now_pntr:= now_pntr^.prv;
 	end;
 end;
procedure tsk1;
 var
	max, now_pntr, prv_pntr: pntr;
 begin
	brdr(); //creates a stack
	writeln();
	now_pntr:= top;
	prv_pntr:= now_pntr;
	if (top = nil) then
		writeln(' <<ERROR: something hapened...')
	else begin
		max:= lmax(); //find lmax
		while (now_pntr <> nil) do begin //for 1 to end
			if (now_pntr^.dat <> max^.dat) and (now_pntr = top) then begin
				top:= now_pntr^.prv;
				dispose(now_pntr);
				now_pntr:= top;
				prv_pntr:= now_pntr;
			end else if (now_pntr^.dat <> max^.dat) then begin
				now_pntr:=now_pntr^.prv;
				dispose(prv_pntr^.prv);
				prv_pntr^.prv:= now_pntr;
			end else begin
				prv_pntr:= now_pntr;
				if (now_pntr <> nil) then
					now_pntr:= now_pntr^.prv; // next element in list
			end;
		end;
		otpt(top);
		writeln();
	end;
 end;
begin
	cls(); //clear screen
	top:= nil; //there are no elements
	tsk1();
end.