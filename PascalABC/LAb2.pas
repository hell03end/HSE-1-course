program lab2;
const lmax=1001;
procedure arcr (var R: array[1..lmax] of real; n: integer);
         var
            i: integer;
            a,x,h: real;
         begin
              write('enter a, x, h to calculate elements of array with r[i] = 0.8cos(8ax-ih): ');
              read(a,x,h);
              for i:= 1 to n do r[i]:= 0.8*cos(8*a*x-i*h);
              for i:= 1 to n do write('{', r[i]:1:5, '}');
              writeln();
         end;
procedure del (var R: array[1..lmax] of real; var n: integer);
         var i, k: integer;
         begin
              k:= 0;
              for i:=1 to n do
                  if abs(r[i]) >= 0.7 then begin
                     k:= k+1;
                     r[k]:= r[i];
                  end;
              if k = 0 then begin
                 writeln('Complete removal.');
                 n:=0;
              end
              else if k = n then writeln('No elements removed.')
              else begin
                   n:= k;
                   for i:= 1 to n do write('{', r[i]:1:5, '}');
                   writeln();
              end;
         end;
procedure avrg (R: array[1..lmax] of real; n:integer);
          var
             t,i,nmin,neg: integer;
             min, answ: real;
          begin
               min:= 1E308;
               nmin:= 1;
               neg:= 1;
               i:= 1;
               answ:=0;
               while (r[i]>0) and (i<=n) do begin
                     inc(i);
                     inc(neg);
               end;
               for i:= 1 to n+1 do
                   if r[i] < min then begin
                      min:= r[i];
                      nmin:= i;
                   end;
               if neg = n+1 then
                  writeln('can not calculate average value, because there are only positive numbers in array.')
               else begin
                    if neg < nmin then begin
                       t:= neg;
                       neg:= nmin;
                       nmin:= t;
                    end;
                    if neg - nmin - 1 <= 0 then writeln('can not be calculated')
                    else if neg - nmin - 1 = 1 then writeln('only one value: ', r[neg-1]:3:5)
                    else begin
                         for i:= nmin+1 to neg-1 do
                             answ:= answ+r[i];
                         answ:= answ / (neg-nmin-1);
                         writeln('average value is: ', answ:3:5);
                    end;
               end;
          end;
var
   R: array[1..lmax] of real;
   n: integer;
begin
     repeat
           write('enter n, n>0, n<', lmax-1, ': ');
           read(n)
     until (n<=(lmax-1)) and (n>0);
     writeln('Task 1.');
     arcr(R, n);
     writeln('Task 2.');
     del(R, n);
     if n = 0 then writeln('...array was fully deleted.')
     else begin
          writeln('Task 3');
          avrg(R, n);
     end;
end.