program lb4_pr1;
const
     nmax = 10;
     Mmax = 20;
var
   a: array[1..nmax, 1..Mmax] of integer;
   b: array[1..nmax] of integer;
   c: array[1..nmax, 1..Mmax] of boolean;
   n,m,k: real;
procedure otpt (n,m: integer);
 var
    i,j: integer;
 begin
      for i:= 1 to n do begin
          for j:= 1 to m do
              write('{', a[i,j], '}');
          writeln();
      end;
 end;
procedure arcr;
 var
    fckng_btch: real;
    i,j: integer;
 begin
      writeln('>>  fill the array (of INTEGER, gdmt!): ');
      for i:= 1 to trunc(n) do
          for j:= 1 to trunc(m) do begin
              read(fckng_btch);
              while (frac(fckng_btch) <> 0) do
                    read(fckng_btch);
              a[i,j]:= trunc(fckng_btch); //
          end;
 end;
procedure brcr;
 var
    fckng_btch: real;
    i: integer;
 begin
      writeln('>>  fill the array (of INTEGER, gdmt!): ');
      for i:= 1 to trunc(k) do begin
          read(fckng_btch);
          while (frac(fckng_btch) <> 0) do
                read(fckng_btch);
          b[i]:= trunc(fckng_btch); //
      end;
 end;
procedure swtch;
 var
    i,j,l,tmp,t: integer;
 begin
{C}   for i:= 1 to trunc(n) do
          for j:= 1 to trunc(m) do
              c[i,j]:= false;
{fnd} for i:= 1 to trunc(n) do
          for j:= 1 to trunc(m) do
              for l:= 1 to trunc(k) do
                  if (b[l] = a[i,j]) then
                     c[i,j]:= true;
{chng}for i:= 1 to trunc(n) do
          for j:= 1 to trunc(m) do
              if (c[i,j] = true) then begin
                 tmp:= a[i,j];
                 t:= 0;
                 while (tmp > 0) do begin
                       t:= t*10 + tmp mod 10;
                       tmp:= tmp div 10;
                 end;
                 a[i,j]:= t;
              end;
 end;
begin
{A}  write('>>  enter n, m (INTEGER, gdmt!): ');
     readln(n,m);
     while (trunc(n) <= 0) or (trunc(n) > nmax) or (trunc(m) <= 0) or (trunc(m) > Mmax) or (frac(n) <> 0) or (frac(m) <> 0) do
           readln(n,m); //
     arcr();
     otpt(trunc(n),trunc(m));
{B}  write('>>  enter k (INTEGER, gdmt!): ');
     readln(k);
     while (trunc(k) <= 0) or (trunc(k) > nmax) or (frac(k) <> 0) do
           readln(k); //
     brcr();
{tsk}swtch();
     otpt(trunc(n),trunc(m));
end.