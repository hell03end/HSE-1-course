//2.	Создать текстовый файл средствами редактора (т.е. в «Блокноте»).
//Прочитать этот файл построчно и произвести в каждой строке следующие действия:
//заменить каждую цифру соответствующей ей латинской буквой
//(‘0’ на ‘A’, ‘1’ на ‘B’ и т.д.).
//3.	Сформировать файл записей, имеющих заданную структуру.
//Количество записей дано. Найти в файле требуемую запись:
//все телевизоры, выпускаемые в данном городе и с размером экрана,
//большим среднего размера для всех телевизоров.

program lb7;	
function inpt: real;
 var
	tmp: String;
	chk: integer;
 begin
	write('>> enter k: ');
	readln(tmp); // read string
	val(tmp,result,chk); // try to transform it into an integer
	while (chk <> 0) or (result <= 0) do begin
		write('  >> enter the value again: ');
		readln(tmp);
		val(tmp,result,chk);
	end;
 end;
procedure otpt (var fl: text);
 var
	tmp: String;
 begin
	reset(fl);
	while not (eof(fl)) do begin
		readln(fl,tmp);
		writeln(tmp);
	end;
 end;
function pth: String; //for usr inpt of the path
 begin
	write('>> enter the path: ');
	readln(result);
	while (CanCreateFile(result + '\1.txt)') <> true) do begin // chk prprts
		writeln('  << can not be created!');
		write('  >> please, enter the path again: ');
		readln(result);
	end;
 end;
procedure kon (var fl: text); // choose to close/save file
 var
	tmp: real;
 begin
	writeln('>> enter [1] to delete file or [2] to save: ');
	tmp:= inpt();
	while (trunc(tmp) <> 1) and (trunc(tmp) <> 2) do
		tmp:= trunc(inpt());
	if (trunc(tmp) = 1) then
		erase(fl); // delete file
 end;
procedure tsk1;
 var
	fl: file of real; //typical file
	i,k: integer;
	tmp,min: real;
	stmp: String;
 begin
	writeln('    Tsk1');
	stmp:= pth + '\tsk1.dat'; //creates file name
	assign(fl,stmp);
	k:= trunc(inpt());
	writeln('>> enter [1] for usr inpt or [2] to rndm: ');
	tmp:= trunc(inpt());
	while (trunc(tmp) <> 1) and (trunc(tmp) <> 2) do
		tmp:= trunc(inpt());
	rewrite(fl);
	for i:= 1 to k do begin
		if (trunc(tmp) = 1) then
{usr}		write(fl,inpt())
		else begin
{chk}		min:= random(100000);
			while (min = 0) do // excpt 0
				min:= random(100000);
			write(fl,power(-1,random(3))*random(1000000000)/min);
		end;
	end;
	reset(fl); //read fl
	min:= maxreal;
	for i:= 1 to k do begin
		read(fl,tmp); // read strng
		if (i mod 2 = 0) and (tmp < min) then begin
			min:= tmp;
			writeln('        << ', tmp:0:3);
		end else
			writeln('  << ', tmp:0:3);
	end;
	close(fl);
	erase(fl); // delete file
	writeln('<< min is: ', min:0:3);
 end;
procedure tsk2;
 var
	fl: text;
	stmp: String;
	i: integer;
 begin
	writeln('    Tsk2');
	stmp:= pth + '\tsk2.txt'; //creates file name
	assign(fl,stmp);
	if (fileexists(stmp)) then
		append(fl)
	else
		rewrite(fl); // creates fl if it isn't excst
	while not (eof(fl)) do begin // until the end
		readln(fl,stmp);
		for i:= 1 to length(stmp) do
			if (stmp[i] in ['0'..'9']) then
				stmp[i]:= chr(ord(stmp[i]) + 17); // change numbers to letters
		// delete previos str
		// writeln(fl, stmp); // write new string in file
	end;
	otpt(fl);
	close(fl);
	kon(fl);
 end;
procedure tsk3;
type options = record
	y: integer; // year
	p,s: real; // p - price, s - size
 end;
type made = record
	c,f: String; // c - city, f - factory
 end;
type str = record
	md: made; // produser
	nm: string; // name
	pr: options;
 end;
 var
	fl: text;
	k: integer;
	stmp: String;
	tv: str;
 begin
	writeln('    Tsk3');
	stmp:= pth + '\tsk3.txt'; //creates file name
	assign(fl,stmp);
	k:= trunc(inpt());
	if (fileexists(stmp)) then
		reset(fl)
	else
		rewrite(fl); // creates fl if it isn't excst
	
	
	close(fl);
	kon(fl);
 end;
begin
	cls(); //clear screen
{1	tsk1();
{2}	tsk2();
{3	tsk3(); {end}
end.