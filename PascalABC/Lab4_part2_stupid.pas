program lb4_pr2;
const
     nmax = 10;
var
   a: array[1..nmax] of integer;
   n: real;
procedure otpt;
 var
    i: integer;
 begin
      for i:= 1 to trunc(n) do
          write('{', a[i], '}');
      writeln();
 end;
procedure sum;
 var
    i,j,tmp: integer;
 begin
      for i:= 1 to trunc(n) do begin
          tmp:= a[i];
          a[i]:= 0;
          while (tmp div 10 <> 0) do begin
                a[i]:= a[i] + tmp mod 10;
                tmp:= tmp div 10;
          end;
          a[i]:= a[i] + tmp;
      end;
      otpt();
 end;
procedure arcr;
 var
    fckng_btch: real;
    i: integer;
 begin
      writeln('fill the array with positive numbers');
      i:= 1;
      readln(fckng_btch);
      while (frac(fckng_btch) <> 0) do
            readln(fckng_btch);
      a[i]:= trunc(fckng_btch);
      while (i < trunc(n)) and (a[i] >= 0) do begin
          inc(i);
          read(fckng_btch);
          while (frac(fckng_btch) <> 0) do
                read(fckng_btch);
          a[i]:= trunc(fckng_btch);
      end;
      if (i = trunc(n)) and (a[i] <> 0) then begin
         {otpt(); writeln();} sum();
      end else
          writeln('only positive numbers!');
 end;
begin
     writeln('Tsk2');
     write('enter n: ');
     readln(n);
     while (n <= 0) or (trunc(n) > nmax) or (frac(n) <> 0) do
           readln(n);
     arcr();
end.