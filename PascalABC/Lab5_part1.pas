program lb5_pr1;
var
   n,k: integer;
procedure rd (var a,b: String; var ta,tb: integer);
 begin
      readln(a);
      ta:= pos(' ',a); //only one value here
      if (ta = 0) then begin //in case if yes:
        readln(b); //then read another value
        ta:= pos(' ',a); //only one value here?
        if (ta <> 0) then //if not then:
          b:= copy(b,1,ta-1); //all other values will be ignored 
      end else begin //in case if not:
        b:= a;
        a:= copy(a,1,ta-1); //first value is a
        delete(b,1,ta); //all what after first is now b
        ta:= pos(' ',b); //is there smth else?
        if (ta <> 0) then begin
          b:= copy(b,1,ta-1); //b is the second value
        end;
      end;
      val(a,n,ta);
      val(b,k,tb);
 end;
procedure inpt;
 var
    a,b: String;
    ta,tb: integer;
 begin
         write('>> enter n, k: ');
         rd(a,b,ta,tb);
         while (ta <> 0) or (tb <> 0) or (n <= 0) or (k < 0) do begin
            write('  >> enter values again: ');
            rd(a,b,ta,tb);
         end;
 end;
function tsk1 (k,n: integer): real;
 var
    i: integer;
 begin
      tsk1:= 1;
      for i:= k to n do
          tsk1:= result * i;
 end;
begin
     writeln('  Tsk 1');
     inpt();
     if (n = 0) or (k = n) then
        writeln('<< a = 1.')
     else if (n - k <= k) then
          writeln('<< a = ', tsk1(k+1,n)/tsk1(1,n-k):1:0, '.')
     else
         writeln('<< a = ', tsk1(n-k+1,n)/tsk1(1,k):1:0, '.');
end.