program lb6;
const
	kmax = 20;
type pntr = ^link;
link = record
     dat: String; //stored data
	 str_nmb,t: integer; //nmbr of basic string
     nxt: pntr; //next element
end;
var
	strwrs: array[1..kmax] of String;
	i,k,chk: integer;
	tmp: String;
	beg,max: pntr; // frst and elmnts of list
procedure fck;
 var
	now_pntr: pntr;
 begin
	now_pntr:= beg;
	max:= beg;
	max^.t:= 0;
	while (now_pntr <> nil) do begin
		now_pntr^.t:= 0;
		for i:= 1 to length(now_pntr^.dat) do
			if (ord(now_pntr^.dat[i]) in [48..57]) then
				inc(now_pntr^.t);
		if (now_pntr^.t > max^.t) then
			max:= now_pntr;
		now_pntr:= now_pntr^.nxt;
	end;
 end;
procedure ot;
 begin
	for i:= 1 to k do // otpt
		writeln('  << [', i, '] {', strwrs[i], '}');
 end;
function rndm: String; //returns random strng
 var
	ii,chk: integer; //lcl "i" and "chk" replace glbl (bad stl)
 begin
	delete(result,1,length(result)); //clear strng 
	chk:= random(254); //lngth of new strng
	for ii:= 1 to chk do
		result:= result + chr(random(255)); //add rndm smbl to strng
 end;
procedure otpt;
 var
	now_pntr: pntr;
 begin
	now_pntr:= beg; //frst element
	if (beg = nil) then //there are no elements in list
		writeln('<< sorry, there is no array.')
	else //in another case
		while (now_pntr <> nil) do begin //for 1 to end
			writeln(' << {', now_pntr^.dat, '}');
			now_pntr:= now_pntr^.nxt; // next element in list
		end;
 end;
procedure arcr;
 var
	i,j: integer;
	now_pntr: pntr;
 begin
	beg:= nil; // there is no lst
	for i:= 1 to k do begin
		chk:= 0; //prv rslt
		for j:= 1 to length(strwrs[i]) do // find rssn lttrs
			if (ord(strwrs[i][j]) in [192..255]) or (strwrs[i][j] = chr(168)) or (strwrs[i][j] = chr(184)) then begin
				if (j - chk <> 1) and (chk <> 0) then begin
{create lst}		if (beg <> nil) then begin //first element in list exist
						new(now_pntr^.nxt); //create next element
						now_pntr:= now_pntr^.nxt; //go to next element
					end else begin //first element in list isn't exist
						new(now_pntr); //create it
						beg:= now_pntr; //and remember it
					end;
					now_pntr^.dat:= copy(strwrs[i],chk+1,j-chk-1);
					now_pntr^.str_nmb:= i; //remember basic strng
				end;
				chk:= j; // nmbr of rssn lttr
			end;
	end;
	if (beg <> nil) then //if array was created
		now_pntr^.nxt:= nil; //lst element
	otpt();
 end;
procedure srt;
 var
	now_pntr,prv_pntr: pntr;
	tmp: String;
	tp: integer;
 begin
	fck();
	prv_pntr:= beg;
    now_pntr:= beg^.nxt;
	while (prv_pntr^.nxt <> nil) do begin
		while (now_pntr <> nil) do begin
			if (now_pntr^.t < prv_pntr^.t) then begin
				tmp:= now_pntr^.dat;
				now_pntr^.dat:= prv_pntr^.dat;
				prv_pntr^.dat:= tmp;
				tp:= now_pntr^.str_nmb;
				now_pntr^.str_nmb:= prv_pntr^.str_nmb;
				prv_pntr^.str_nmb:= tp;
			end;
			now_pntr:= now_pntr^.nxt;
		end;
		prv_pntr:= prv_pntr^.nxt;
		now_pntr:= prv_pntr^.nxt;
	end;
	writeln(); writeln('**************** max ****************'); writeln();
	otpt();
	fck();
 end;
procedure del;
 begin
	delete(strwrs[max^.str_nmb],pos(max^.dat,strwrs[max^.str_nmb]),length(max^.dat));
	ot();
 end;
begin
	cls();
	write('>> enter k: ');
	readln(tmp); // read string
	val(tmp,k,chk); // try to transform it into an integer
	while (chk <> 0) or (k > kmax) or (k <= 0) do begin
		write('  >> enter the value again: ');
		readln(tmp);
		val(tmp,k,chk);
	end;
	write('>> enter [1] for usr inpt or [2] to rndm: ');
	readln(tmp);
	val(tmp,chk,i); // try to transform it into an integer
	while (i <> 0) or (chk > 2) or (chk <= 0) do begin
		write('  >> enter [1] or [2] again: ');
		readln(tmp);
		val(tmp,chk,i);
	end;
	for i:= 1 to k do begin // inpt of array
{usr}	if (chk = 1) then begin
			write('>> enter (', i, ') line: ');
			readln(strwrs[i]);
		end else
{chk}		strwrs[i]:= rndm(); {}
	end;
	ot();
{1}	arcr();
	if (beg <> nil) then begin //if array exist
{2}		srt();
{3}		del(); {end}
	end else
		writeln('   End.');
end.